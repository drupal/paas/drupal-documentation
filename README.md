# CERN Drupal Documentation

![Build Status](https://gitlab.com/pages/docusaurus/badges/master/pipeline.svg)

---

Welcome to the CERN Drupal Distribution hosted at https://drupal.docs.cern.ch/!

We strive to make these docs your go-to place for anything Drupal.

We are continuously expanding the website with guides and tutorials for most use-cases.

If you have any questions or concerns, please refer to the CERN Drupal Community forum.

## Make changes locally and test

If you want to make changes to docs and see the changes happening locally
before pushing the changes, simply run:
```bash
docker run -it --network=host -v <PathToDocs>/drupal-documentation/docs:/test -w /test node:16 bash
# Once inside the container
## If you are runinng for the first time
npm install
## To start serving the content
npm start
# Now dev website should be avaialble on localhost:3000/
````

## How can I help?

This documentation is open and accessible by everyone with a CERN account. 
If you spot an error, a typo, or would like to propose changes, please click _Edit this page_ at the bottom of any page. 
This allows you to submit your proposed changes to this repository. 
We will then review your submissions and update the documentation accordingly.

## Contact

This documentation is maintained by the IT Drupal Infrastructure team and the Web Team.
