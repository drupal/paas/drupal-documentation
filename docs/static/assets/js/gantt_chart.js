export function onRouteDidUpdate({location, previousLocation}) {

  function createGanttChart() {
    const canvas = document.createElement("canvas");
    canvas.width = 900;
    canvas.height = 450;

    const container =
      document.getElementById("gantt-chart-container") || document.body;
    container.appendChild(canvas);

    const ctx = canvas.getContext("2d");

    // Enhanced color palette
    const colors = ["#FF9AA2", "#FFB7B2", "#FFDAC1", "#E2F0CB", "#B5EAD7"];
    const backgroundColor = "#F7F7F7";
    const gridColor = "#E0E0E0";
    const textColor = "#333333";

    const milestones = [
      {
        name: "WordPress Lite",
        start: new Date("2024-10-28"),
        end: new Date("2025-05-31"),
        internal: false,
      },
      {
        name: "WordPress MVP",
        start: new Date("2025-01-01"),
        end: new Date("2025-02-31"),
        internal: true,
      },
      {
        name: "WordPress Pilot",
        start: new Date("2025-03-01"),
        end: new Date("2025-04-31"),
        internal: true,
      },
      {
        name: "WordPress",
        start: new Date("2025-05-01"),
        end: new Date("2027-02-31"),
        internal: false,
      },
      {
        name: "Migration",
        start: new Date("2025-07-01"),
        end: new Date("2026-01-31"),
        internal: false,
      },
    ];

    const margin = {top: 70, right: 30, bottom: 80, left: 180};
    const width = canvas.width - margin.left - margin.right;
    const height = canvas.height - margin.top - margin.bottom;

    const startDate = new Date("2024-08-01");
    const endDate = new Date("2026-01-31");
    endDate.setMonth(endDate.getMonth() + 1);
    const timeScale = (date) =>
      ((date - startDate) / (endDate - startDate)) * width;

    // Draw chart background
    ctx.fillStyle = backgroundColor;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    // Draw grid
    ctx.strokeStyle = gridColor;
    ctx.lineWidth = 1;

    const monthsBetween =
      (endDate.getFullYear() - startDate.getFullYear()) * 12 +
      endDate.getMonth() -
      startDate.getMonth();
    for (let i = 0; i <= monthsBetween; i++) {
      const x = margin.left + (i / monthsBetween) * width;
      ctx.beginPath();
      ctx.moveTo(x, margin.top);
      ctx.lineTo(x, height + margin.top);
      ctx.stroke();
    }

    const rowHeight = height / 5;
    for (let i = 0; i <= 5; i++) {
      const y = margin.top + i * rowHeight;
      ctx.beginPath();
      ctx.moveTo(margin.left, y);
      ctx.lineTo(width + margin.left, y);
      ctx.stroke();
    }

    // Draw title
    ctx.font = "bold 28px Arial";
    ctx.fillStyle = textColor;
    ctx.textAlign = "center";
    ctx.fillText("WordPress Roadmap", canvas.width / 2, 40);

    // Draw x-axis labels
    ctx.font = "12px Arial";
    ctx.fillStyle = textColor;
    ctx.textAlign = "right";
    ctx.save();
    for (let i = 0; i <= monthsBetween; i++) {
      const date = new Date(startDate.getFullYear(), startDate.getMonth() + i, 1);
      const x = margin.left + timeScale(date);
      ctx.translate(x, height + margin.top + 15);
      ctx.rotate(-Math.PI / 4);
      ctx.fillText(
        date.toLocaleString("default", {month: "short", year: "2-digit"}),
        0,
        0,
      );
      ctx.rotate(Math.PI / 4);
      ctx.translate(-x, -(height + margin.top + 15));
    }
    ctx.restore();

    // Draw milestones
    milestones.forEach((milestone, index) => {
      const y = margin.top + index * rowHeight + rowHeight / 2;
      const x1 = margin.left + timeScale(milestone.start);
      const x2 = margin.left + timeScale(milestone.end);

      // Draw milestone bar with rounded corners
      ctx.fillStyle = colors[index % colors.length];
      ctx.beginPath();
      ctx.moveTo(x1 + 5, y - 15);
      ctx.lineTo(x2 - 5, y - 15);
      ctx.quadraticCurveTo(x2, y - 15, x2, y - 10);
      ctx.lineTo(x2, y + 10);
      ctx.quadraticCurveTo(x2, y + 15, x2 - 5, y + 15);
      ctx.lineTo(x1 + 5, y + 15);
      ctx.quadraticCurveTo(x1, y + 15, x1, y + 10);
      ctx.lineTo(x1, y - 10);
      ctx.quadraticCurveTo(x1, y - 15, x1 + 5, y - 15);
      ctx.fill();

      // Draw milestone name
      ctx.font = "bold 14px Arial";
      ctx.fillStyle = textColor;
      ctx.textAlign = "right";
      ctx.fillText(milestone.name, margin.left - 15, y + 5);

      // Draw "Internal Only" label if applicable
      if (milestone.internal) {
        ctx.font = "italic 10px Arial";
        ctx.fillStyle = "#666666";
        ctx.fillText("Internal Only", margin.left - 15, y + 20);
      }
    });

    // Draw today's date line
    const today = new Date();
    const todayX = margin.left + timeScale(today);
    ctx.strokeStyle = "#FF6B6B";
    ctx.lineWidth = 2;
    ctx.setLineDash([5, 5]);
    ctx.beginPath();
    ctx.moveTo(todayX, margin.top);
    ctx.lineTo(todayX, height + margin.top);
    ctx.stroke();
    ctx.setLineDash([]);

    // Add centered legend for today's date
    ctx.font = "bold 14px Arial";
    ctx.fillStyle = "#FF6B6B";
    ctx.textAlign = "center";
    ctx.fillText("Today", todayX, margin.top - 15);

    // Add a subtle border to the chart
    ctx.strokeStyle = "#D0D0D0";
    ctx.lineWidth = 2;
    ctx.strokeRect(1, 1, canvas.width - 2, canvas.height - 2);

    // Add tiny line in the bottom right
    ctx.font = "8px Arial";
    ctx.fillStyle = textColor;
    ctx.textAlign = "right";
    ctx.fillText(
      `Generated ${new Date().toLocaleDateString()}`,
      canvas.width - 10,
      canvas.height - 10,
    );
  }

  if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", createGanttChart);
  } else {
      if (location.pathname !== previousLocation?.pathname && location.pathname === "/roadmap-wordpress") {
        createGanttChart();
      }
  }
}