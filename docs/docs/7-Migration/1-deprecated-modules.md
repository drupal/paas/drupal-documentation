---
slug: /deprecated-modules
---

# Deprecated Modules

As part of all upgrades and migrations, we upgrade the modules included in the CERN Drupal Distribution.
As outlined in [Introduction to Modules](/modules), we always require community-contributed (i.e. third-party) modules to have a proven record and otherwise be actively supported by multiple developers.
Accordingly, in the event that a module stops receiving support, fails to address potential security vulnerabilities, or eventually becomes incompatible with newer versions of Drupal, we may be forced to remove it from the distribution.

## Ludwig

Ludwig was officially created in the middle of 2017 by Drupal. It was the standard for module management at the time.
Since Drupal 8 is released, the preferred tool for Drupal site management is Composer [ref](https://www.drupal.org/docs/contributed-modules/ludwig/why-ludwig#s-external-php-libraries-management). Composer takes care of all the required libraries nicely and automatically.

> Can I use both Composer and Ludwig in my project?
No. We use composer to manage all the PHP libraries. All available [documentation](https://www.drupal.org/docs/contributed-modules/ludwig/why-ludwig#s-can-i-use-both-composer-and-ludwig-in-my-project) reports we can only support one or the other. They are not compatible, so in short, you shouldn't use it (or need it)!

## PHP 8.1
The following modules **were not compatible with PHP 8.1 and were removed from the CERN Drupal Distribution**:

| module | link | version |
|--------|------|---------|
|workbench|[drupal/workbench](https://drupal.org/project/workbench)|~1.3.0|
|workbench_access|[drupal/workbench_access](https://drupal.org/project/workbench_access)|~1.0@beta|
|workbench_moderation|[drupal/workbench_moderation](https://drupal.org/project/workbench_moderation)|~1.6.0|
|sticky|[drupal/sticky](https://drupal.org/project/sticky)|~2.0.0|
|panelizer|[drupal/panelizer](https://drupal.org/project/panelizer)|~4.4.0|
|hotjar|[drupal/hotjar](https://drupal.org/project/hotjar)|~2.2.0|
|fast_404|[drupal/fast_404](https://drupal.org/project/fast_404)|~2.0@alpha|
|context|[drupal/context](https://drupal.org/project/context)|~4.1.0|
|contribute|[drupal/contribute](https://drupal.org/project/contribute)|~1.x-dev@dev|
|google_analytics|[drupal/google_analytics](https://www.drupal.org/project/google_analytics)|~3.1|

If your website strictly relies on any of these modules, it is possible to assume ownership of the module(s) and apply the necessary changes to ensure continued compatibility. 
If you are unable to replace the module(s) with an alternative, and cannot assume responsibility for ensuring continued compatibility, please get in touch.
