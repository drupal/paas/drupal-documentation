---
slug: /modules
---


# Introduction to Modules

In the context of Drupal, a module is a software package which provides functionality not offered through Drupal itself.
At CERN, this could be integration with CDS (https://cds.cern.ch/) to host media or Indico (https://indico.cern.ch/) to import and display events.
By default, all CERN websites include a collection of CERN-specific modules through the centrally managed CERN Drupal Distribution.
In addition to official CERN modules developed and maintained by the Web Team, the distribution also includes a collection of carefully curated community contributed modules, ensuring that the most common use-cases are accommodated out-of-the-box.

:::info
We differentiate between three types of modules:

1. CERN-official modules;
2. Community-contributed modules included in the CERN Drupal Distribution; and

This section will cover **CERN-official modules only**.
:::

Included in the CERN Drupal Distribution are the following CERN-official modules:

- [drupal/cern-cds-media](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-components](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-dev-status](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-display-formats](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-full-html-format](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-indico-feeds](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-install-profiles](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-integration](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-landing-page](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-ldap-api](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-loading](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-paragraph-types](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-profile-displayname](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-toolbar](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)
- [drupal/cern-webcast-feeds](https://gitlab.cern.ch/web-team/drupal/public/d10/modules)   

Additional modules exist as underlying dependencies and cannot be configured directly.
In the following pages, some of the above CERN-official modules will be covered in greater detail.
If you are looking for information about a specific module, kindly refer to the sidebar for quickly navigating to the correct page.
Should you have any questions about a CERN-official module _not_ otherwise covered in this documentation, please reach out.

## Centrally Provided Modules

In addition to the CERN-official modules, a selection of carefully curated modules are included in the CERN Drupal Distribution.
These modules are either recommended by the Drupal Association, widely used across CERN, or act as a dependency for CERN-offical modules.
This list is not exhaustive, and modules may be added or removed as our offering advances.
As these are community-contributed (i.e. third-party modules), we always require modules to have a proven record and otherwise be actively supported by multiple developers.
In the event that a module stops receiving support, fails to address potential security vulnerabilities, or eventually becomes incompatible with newer versions of Drupal, we may be forced to remove it from the distribution.

As of November 2022, centrally provided modules include:

- [bower-asset/bootstrap](https://www.drupal.org/project/bootstrap)
- [bower-asset/ckeditor](https://www.drupal.org/project/ckeditor)
- [bower-asset/jquery-cycle](https://asset-packagist.org/package/bower-asset/jquery-cycle)
- [bower-asset/jquery-hoverintent](https://asset-packagist.org/package/bower-asset/jquery-hoverintent)
- [bower-asset/json2](https://www.drupal.org/project/json2)
- [ckeditor/ckeditor](https://www.drupal.org/project/ckeditor)
- [composer/installers](https://github.com/composer/installers)
- [cweagans/composer-patches](https://github.com/cweagans/composer-patches)
- [dompdf/dompdf](https://www.drupal.org/project/dompdf)
- [drupal/admin_toolbar](https://www.drupal.org/project/admin_toolbar)
- [drupal/adminimal_theme](https://www.drupal.org/project/adminimal_theme)
- [drupal/allowed_formats](https://www.drupal.org/project/allowed_formats)
- [drupal/bootstrap](https://www.drupal.org/project/bootstrap)
- [drupal/captcha](https://www.drupal.org/project/captcha) 
- [drupal/ckeditor_font](https://www.drupal.org/project/ckeditor_font)
- [drupal/codesnippet](https://www.drupal.org/project/codesnippet)
- [drupal/color_field](https://www.drupal.org/project/color_field)
- [drupal/colorbox](https://www.drupal.org/project/colorbox)
- [drupal/colorbutton](https://www.drupal.org/project/colorbutton)
- [drupal/components](https://www.drupal.org/project/components)
- [drupal/config_update](https://www.drupal.org/project/config_update)
- [drupal/content_access](https://www.drupal.org/project/content_access)
- [drupal/cookieconsent](https://www.drupal.org/project/cookieconsent)
- [drupal/core-composer-scaffold](https://www.drupal.org/project/drupal/releases/)
- [drupal/core-dev](https://www.drupal.org/project/drupal/releases/)
- [drupal/core-project-message](https://www.drupal.org/project/drupal/releases/)
- [drupal/core-recommended](https://www.drupal.org/project/drupal/releases/)
- [drupal/ctools](https://www.drupal.org/project/ctools)
- [drupal/devel](https://www.drupal.org/project/devel)
- [drupal/ds](https://www.drupal.org/project/ds)
- [drupal/easy_breadcrumb](https://www.drupal.org/project/easy_breadcrumb)
- [drupal/entity_browser](https://www.drupal.org/project/entity_browser)
- [drupal/entity_print](https://www.drupal.org/project/entity_print)
- [drupal/entity_reference_revisions](https://www.drupal.org/project/entity_reference_revisions)
- [drupal/externalauth](https://www.drupal.org/project/externalauth)
- [drupal/extlink](https://www.drupal.org/project/extlink)
- [drupal/facets](https://www.drupal.org/project/facets)
- [drupal/features](https://www.drupal.org/project/features)
- [drupal/feeds](https://www.drupal.org/project/feeds)
- [drupal/fences](https://www.drupal.org/project/fences)
- [drupal/field_formatter_class](https://www.drupal.org/project/field_formatter_class)
- [drupal/field_group](https://www.drupal.org/project/field_group)
- [drupal/field_permissions](https://www.drupal.org/project/field_permissions)
- [drupal/filefield_paths](https://www.drupal.org/project/filefield_paths)
- [drupal/filelog](https://www.drupal.org/project/filelog)
- [drupal/honeypot](https://www.drupal.org/project/honeypot)
- [drupal/hook_event_dispatcher](https://www.drupal.org/project/hook_event_dispatcher)
- [drupal/imagemagick](https://www.drupal.org/project/imagemagick)
- [drupal/jquery_ui](https://www.drupal.org/project/jquery_ui)
- [drupal/jquery_ui_accordion](https://www.drupal.org/project/jquery_ui_accordion)
- [drupal/jquery_ui_datepicker](https://www.drupal.org/project/jquery_ui_datepicker)
- [drupal/jquery_ui_draggable](https://www.drupal.org/project/jquery_ui_draggable)
- [drupal/jquery_ui_droppable](https://www.drupal.org/project/jquery_ui_droppable)
- [drupal/mailsystem](https://www.drupal.org/project/mailsystem)
- [drupal/matomo](https://www.drupal.org/project/matomo)
- [drupal/memcache](https://www.drupal.org/project/memcache)
- [drupal/menu_block](https://www.drupal.org/project/menu_block)
- [drupal/menu_breadcrumb](https://www.drupal.org/project/menu_breadcrumb)
- [drupal/menu_force](https://www.drupal.org/project/menu_force)
- [drupal/metatag](https://www.drupal.org/project/metatag)
- [drupal/migrate_plus](https://www.drupal.org/project/migrate_plus)
- [drupal/migrate_tools](https://www.drupal.org/project/migrate_tools)
- [drupal/migrate_upgrade](https://www.drupal.org/project/migrate_upgrade)
- [drupal/module_filter](https://www.drupal.org/project/module_filter)
- [drupal/node_view_permissions](https://www.drupal.org/project/node_view_permissions)
- [drupal/openid_connect](https://www.drupal.org/project/openid_connect)
- [drupal/panelbutton](https://www.drupal.org/project/panelbutton)
- [drupal/panels](https://www.drupal.org/project/panels)
- [drupal/paragraphs](https://www.drupal.org/project/paragraphs)
- [drupal/pathauto](https://www.drupal.org/project/pathauto)
- [drupal/permissions_by_term](https://www.drupal.org/project/permissions_by_term)
- [drupal/r4032login](https://www.drupal.org/project/r4032login)
- [drupal/recaptcha](https://www.drupal.org/project/recaptcha)
- [drupal/redirect](https://www.drupal.org/project/redirect)
- [drupal/redis](https://www.drupal.org/project/redis)
- [drupal/require_login](https://www.drupal.org/project/require_login)
- [drupal/rules](https://www.drupal.org/project/rules)
- [drupal/scheduler](https://www.drupal.org/project/scheduler)
- [drupal/search_api](https://www.drupal.org/project/search_api)
- [drupal/smart_trim](https://www.drupal.org/project/smart_trim)
- [drupal/token](https://www.drupal.org/project/token)
- [drupal/twig_tweak](https://www.drupal.org/project/twig_tweak)
- [drupal/ui_patterns](https://www.drupal.org/project/ui_patterns)
- [drupal/ui_patterns_ds](https://www.drupal.org/project/ui_patterns)
- [drupal/ui_patterns_field_group](https://www.drupal.org/project/ui_patterns)
- [drupal/ui_patterns_layouts](https://www.drupal.org/project/ui_patterns)
- [drupal/ui_patterns_library](https://www.drupal.org/project/ui_patterns)
- [drupal/upgrade_status](https://www.drupal.org/project/upgrade_status)
- [drupal/userprotect](https://www.drupal.org/project/userprotect)
- [drupal/views_bulk_operations](https://www.drupal.org/project/views_bulk_operations)
- [drupal/views_slideshow](https://www.drupal.org/project/views_slideshow)
- [drupal/views_taxonomy_term_name_depth](https://www.drupal.org/project/views_taxonomy_term_name_depth)
- [drupal/viewsreference](https://www.drupal.org/project/viewsreference)
- [drupal/webform](https://www.drupal.org/project/webform)
- [drush/drush](https://www.drupal.org/project/drush)
- [npm-asset/jquery](https://www.drupal.org/project/jquery)
- [vlucas/phpdotenv](https://github.com/vlucas/phpdotenv)
- [webflo/drupal-finder](https://github.com/webflo/drupal-finder)
- [wikimedia/composer-merge-plugin](https://github.com/wikimedia/composer-merge-plugin)

If you feel a module should be included in the CERN-wide offering, please get in touch.
