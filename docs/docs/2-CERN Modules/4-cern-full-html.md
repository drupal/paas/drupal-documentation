---
slug: /modules/cern-full-html
---

# CERN Full HTML

CERN Full HTML is an enriched text format included in the CERN Drupal Distribution.

## Text Styles

The following styles are made available via the CERN Full HTML module.

### Cleaners Styles
The ‘Cleaners Styles’ remove the paragraphs and styles applied to text or images to make sure it sits nicely.

<table>
	<thead>
		<tr>
			<th>Style</th>
			<th>Description</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td data-title="Account" scope="col"><strong>Style</strong></td>
			<td data-title="Description"><strong>Description</strong></td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td><b>Break columns</b></td>
			<td>
				If you are building text in columns with aligned images or blockquotes, add break columns to continue working with the text in a single column. 
				This breaks the column structure you have applied to the text.
			</td>
		</tr>
		<tr>
			<td><b>Clear Paragraphs styles</b></td>
			<td>
				This removes the styles applied, e.g. columns or blockquotes. You can use it to get text to wrap around an image.
			</td>
		</tr>
		<tr>
			<td><b>Clear Blockquote styles</b></td>
			<td>
				Required if the blockquote re-arranges the lines from the quote across the entire page. 
				If you previously added an style to a paragraph or to a blockquote, it remove these styles.
			</td>
		</tr>
		<tr>
			<td><b>Hide paragraph in mobile view</b></td>
			<td>
				In headers, <code>br</code> tags or paragraphs are often used to align the text more centrally. 
				However, this does not translate well to smaller screens. 
				Add this style to the paragraphs so that they look nice in mobile view.
			</td>
		</tr>
	</tbody>
</table>

### Column Paragraphs
The column paragraph styles are vital for putting the text into the correct width for users to scan it. 
All text in every Story and Landing Page should have one of these columns applied.

<table>
	<thead>
		<tr>
			<th>Style</th>
			<th>Description</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td data-title="Style" scope="col"><strong>Style</strong></td>
			<td data-title="Description"><strong>Description</strong></td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td>
			<h4><b>Right Col. / Breakout IMG</b></h4>
			</td>
			<td>
				This puts the text on the right, with the image on the left breakout out of the margins.
				You need to apply the class <code>breakout-right</code> to CDS images.
			</td>
		</tr>
		<tr>
			<td><b>Left Col. / Breakout IMG</b></td>
			<td>
				This puts the text on the left and an image floating on the right. 
				You must also apply the style <code>breakout-right</code> to CDS images.
			</td>
		</tr>
		<tr>
			<td><b>Right Col. / align IMG</b></td>
			<td>
				This positions the text on the right, with an image in a column to the left, aligned with the text. 
				It makes the text column slightly narrower as the image is more central.
			</td>
		</tr>
		<tr>
			<td><b>Left Col. / align IMG</b></td>
			<td>
				This aligns the text on the left, with an image on the right with the CSS class <code>align-right</code> applied.
			</td>
		</tr>
	</tbody>
</table>

### Block Quotes
These styles align blockquotes into the column structure.

<table>
	<thead>
		<tr>
			<th>Style</th>
			<th>Description</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td data-title="Style" scope="col"><strong>Style</strong></td>
			<td data-title="Description"><strong>Description</strong></td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td><b>Right BQ / breakout IMG</b></td>
			<td>Places text and blockquote on the right and image on the left.</td>
		</tr>
		<tr>
			<td><b>Left BQ / breakout IMG</b></td>
			<td>Places text and blockquote on the left and image on the right.</td>
		</tr>
		<tr>
			<td><b>Right BQ / align IMG</b></td>
			<td>Places text and blockquote on the right and image on the left, while image and text are aligned.</td>
		</tr>
		<tr>
			<td><b>Left BQ / align IMG</b></td>
			<td>By selecting the text, and applying ‘left BQ/ align IMG’ you can have a blockquote next to an image.</td>
		</tr>
	</tbody>
</table>
These styles align blockquotes into the column structure.


<table>
	<thead>
		<tr>
			<th>Style</th>
			<th>Description</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td data-title="Style" scope="col"><strong>Style</strong></td>
			<td data-title="Description"><strong>Description</strong></td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td><strong>Small margin left separator columns</strong></td>
			<td>
				Adds a small margin left to the paragraph (more or less <code>30px</code> wide) if you want to add a margin in a column next to to another column. 
				Useful for lists next to an image.
			</td>
		</tr>
		<tr>
			<td><b>hide-inline-text-in-mobile</b></td>
			<td>
				Like Hide paragraphs, it is used for example in the text of headers, where you add lots of breaklines before the text.
				Use this style to hide those breaklines in mobile view.
			</td>
		</tr>
		<tr>
			<td><b>Tags</b></td>
			<td>Adds a highlight effect behind the selected texts, just like tags.</td>
		</tr>
		<tr>
			<td><b>UPPERCASE TAGS</b></td>
			<td>Add the art direction of a tag to a piece of text (use <code>span</code> tag). The highlighted colour is the colour set in the theme settings.</td>
		</tr>
		<tr>
			<td><b>Custom lines</b></td>
			<td>Adds a long line into the text. Most commonly used in the hero header on the homepage to break up the questions.</td>
		</tr>
	</tbody>
</table>

These add the column style to a bullet point list.

<table>
	<thead>
		<tr>
			<th>Style</th>
			<th>Description</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<td data-title="Style" scope="col"><strong>Style</strong></td>
			<td data-title="Description"><strong>Description</strong></td>
		</tr>
	</tfoot>
	<tbody>
		<tr>
			<td><b>Right UL / breakout IMG</b></td>
			<td>Breaks-out an image on the left when the bullet point list in on the right.</td>
		</tr>
		<tr>
			<td>
			<b>Left UL / breakout IMG</b>
			</td>
			<td>Breaks-out an image on the right when the bullet point list in on the left.</td>
		</tr>
		<tr>
			<td><b>Right UL / align IMG</b></td>
			<td>Breaks-out an image on the left when the bullet point list in on the right and aligns the image with the list.</td>
		</tr>
		<tr>
			<td><b>Left UL / align IMG</b></td>
			<td>Breaks-out an image on the right when the bullet point list in on the left and aligns the image with the list.</td>
		</tr>
	</tbody>
</table>
