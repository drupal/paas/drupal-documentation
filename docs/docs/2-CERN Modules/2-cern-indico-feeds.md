---
slug: /modules/cern-indico-feeds
---

# CERN Indico Feeds

The **CERN Indico Events** and **CERN Indico Feeds** modules are offered as part of the CERN Drupal Distribution and allows you to import feeds from Indico directly onto your Drupal website(s) and display the associated events.
The CERN Indico Events module introduces a dedicated content type and views for Indico feeds, and the CERN Indico Feeds module features functionality to integrate against the Indico API (see more at https://docs.getindico.io/en/stable/http-api/).

:::info
While the CERN Indico Feeds module is readily available via [Gitlab](https://gitlab.cern.ch/web-team/drupal/public/d9/modules/cern-indico-feeds), it is included in the CERN Drupal Distribution by default. Thus, it should already be installed on your website and a local installation is not necessary. If for some reason you _are_ using a locally installed copy, please ensure to always use the newest available version!
:::

::warn
This guide utilises **API Keys** which will be **deprecated** in a future Indico release.
As a consequence, we will update the CERN Indicio Feeds module to support token-based authentication.
This means that with a future release, you must enter a token instead of an API key.
No other changes are expected.
:::

## Configuring CERN Indico Feeds

0. Ensure that both the CERN Indico Events and CERN Indico Feeds modules are installed and enabled.

1. Head to **Structure** and select **Feed types**.

![Selecting Feeds type via the Structure menu.](/assets/img/2-cern-modules/cern_indico_feeds_1.png)

<p className="image-caption-shadow">Selecting Feeds type via the Structure menu.</p>

2. Select **Indico Feed** and click **Edit**.

![Editing Indico Feed via the _Feeds type_ administration.](/assets/img/2-cern-modules/cern_indico_feeds_2.png)

<p className="image-caption-shadow">Editing Indico Feed via the Feeds type administration.</p>

This page contains general settings for the CERN Indico Feed type.

- **Settings** defines the import period (i.e. the frequency with which feeds should be updated).
  The default value is six hours and should be sufficient for the vast majority of websites.
  If you require very frequent updates (i.e. every hour or several times an hour), consider linking users directly to Indico instead.
- **Fetcher settings** provides an interface to manage authentication with the Indico API.
- **Processor settings** provides an interface to manage how existing content should be updated, what happens with previously imported items, when content should expire, and who should be the owner of the created items.
  The default settings should meet the requirements for the vast majority of websites. Only advanced users should change processor settings.

3. Select **Fetcher settings**.

![Selecting Fetcher settings when editing Indico Feed.](/assets/img/2-cern-modules/cern_indico_feeds_3.png)

<p className="image-caption-shadow">Selecting Fetcher settings when editing Indico Feed.</p>

On here, we need to update the **API Key** and **Secret Key**. This step needs to be completed once.

4. Head to https://indico.cern.ch/user/api/ and sign in. Click **Create API key**.

![Creating an API Key for Indico.](/assets/img/2-cern-modules/cern_indico_feeds_4.png)

<p className="image-caption-shadow">Creating an API Key for Indico.</p>

This will load the following page. **Do not share your keys with anyone!**

![Viewing a newly created API Key for Indico.](/assets/img/2-cern-modules/cern_indico_feeds_5.png)

<p className="image-caption-shadow">Viewing a newly created API Key for Indico.</p>

5. Copy the **Token** and paste it as your **API Key**.

Copy the **Secret** and paste it as your **Secret Key**.

![Pasting an API Key to update Fetcher settings.](/assets/img/2-cern-modules/cern_indico_feeds_6.png)

<p className="image-caption-shadow">Pasting an API Key to update Fetcher settings.</p>

6. Click **Save feed type**. Your website can now make authenticated calls to the Indico service.

## Creating a new Feed

In order to create a new feed and start pulling events from Indico, follow the below steps.

_Kindly note that the CERN Indico Events content type does not include all the fields from Indico._
_If you require a field that is not included, you must add it manually in the content type and otherwise map it to the Indico field in the Mapping section of the feed._
_This is done by going to Structure → Feed types → Edit Indico Feed → Mapping tab._

1. Go to **Content** and click **Feeds**.

![Creating a new feed.](/assets/img/2-cern-modules/cern_indico_feeds_7.png)

<p className="image-caption-shadow">Creating a new feed.</p>

2. Click **Add feed**.

![Creating a new feed.](/assets/img/2-cern-modules/cern_indico_feeds_8.png)

<p className="image-caption-shadow">Creating a new feed.</p>

3. Add information describing your feed.

![Creating a new feed.](/assets/img/2-cern-modules/cern_indico_feeds_9.png)

<p className="image-caption-shadow">Creating a new feed.</p>

- **Title** is the title of your feed.
- **Categories** is the list of Indico category ids from which you wish to pull events. If, for instance, you put category id 1279 (Schools), the module will pull all events from the child categories under it (namely CERN Official Schools and Other Schools). In the event that you are only interested in a specific category, please put the specific id. There is no limitation on the number of categories from which the module can pull.
- **Start date** is the date from which the module should start pulling events from Indico.
- **End date** is the latest date from which the module should pull events from Indico.
- **Room** should be left empty unless strictly necessary.
- **Retrieve only public events** should be ticked by default.
  If you require private events from the Indico categories, untick this accordingly.
  Kindly note that private events will only be visible to users who have access to see them in the first place and have signed in on your website.
  Additionally, if you require private events, please ensure that the account from which you generated API keys has access to the events.
- **Import options** should be left unchanged.
  Unticking this option will stop the pulling of events.
- **Authoring information** should be left unchanged.

Once done, click **Save and import**.

Imported events can now be displayed using Grid of event boxes, Collisions of events, or Countdown to next event content types.
