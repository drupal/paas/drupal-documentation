---
slug: /modules/cern-landing-page
---

# CERN Landing Page

A `Landing Page` constitutes a content type that provides standalone pages to add in your Drupal site in order to provide a nice way to present and advertise your page and content. 
`Landing Pages` consist of building blocks, which can be web components, views, or blocks. 
Regardless of type (i.e. component / view / block), `Landing Pages` are are always encapsulated in Section wrappers.
The below screenshot shows an example of a `Landing Page`.

![The home.cern website.](/assets/img/2-cern-modules/landing-page.png)
<p className="image-caption-shadow">The home.cern website.</p>

In order to enable `Landing Page` content type, you have to enable the **CERN Landing Page** module.
This comes pre-installed and pre-enabled by default when using the CERN Drupal Distribution.
Importantly, `Landing Pages` are Drupal nodes like other types of content. 
As a result, a landing page can be created by navigating to `Content > Add Content > Landing Page`.

The `Landing Page` contains the following elements:

## Section Wrapper

All web components are encapsulated inside _sets divisions_ called `Sections`. 

The following image presents the concept of section in a more visual way. 
More specifically, each red frame of the screenshot indicates one section. 
Sections are useful to configure settings for the content inside. 
For example, some typical settings can be the `height` of the section, the `width` of the section, or whether you want to apply `parallax` effects while scrolling the section. 
The paragraphs below explain all the settings in details.

![Landing Page section wrappers.](/assets/img/2-cern-modules/landing-page-sections.png)
<p className="image-caption">Landing Page section wrappers on home.cern.</p>

### Basic Settings

Each section has the following, configurable settings:

|**Setting**|**Description**|
|--------|------------|
|**Has header?**|This option is meant to be used with the Hero Header component. More specifically, when this option is checked, the menu is displayed on the Hero Header instead of above the Hero Header.|
|**Title/ Title color/ Show title**|The title of the section along with settings regarding its color and whether you want it to be rendered or not.|
|**Scrolling Menu Title / Show Scrolling Menu Title**|This is meant to be used withthe Scrolling Menu component. More specifically, in this setting you canset the title that you want this section to have on the Scrolling Menu and whether you want it be rendered it or not.|
|**Display**|This setting sets the width of the Section:<br /><br />**Centered Content**: The content and the background of the section are centered.<br />**Fluid width**: The content and the background of the section cover the full width of the page.<br />**Fluid BG Center content**: The background of the section covers the full width of the page but the content is centered.|
|**Section height**|This setting sets the height of the Section:<br /><br />**-None-**: In this case, the section will cover as much content as necessary for the content to be displayed.<br />**Half Height**: The Section forcely covers half of the viewport. <br />**Full Height**: The Section forcely covers the whole viewport.|
|**Effects**|This setting provides a set of effects for the sections, mostly related to the parallax scrolling effect. <br /><br />**Background parallax**: While scrolling, the section will move behind the next section. <br />**Cover parallax**: While scrolling, the section will cover the next section. <br />**Cover parallax video compatible**: The same as cover parallax, but includes compatibility for CDS videos. <br />**Background rotation**: while scrolling the background will be rotating.|
|**Lazy Section**|This setting makes the section appear while scrolling|
|**Box effects**|This setting is meant to be used for the 4 box elements: Article Box, Thumbnail Box, Resource Box and Agenda Box. More specifically, it provides an effect that expands each box when hovering. In order to be used, all the components inside this section need to be of box type.|

### Background Settings

Any section can have a background. 
This background can be an image, a video, or a colour. 
In the cases of images and videos, they can be either local resources that you upload from your machine or CDS resources.
If you are choosing an image, be mindful of the size of the image as this would need to be loaded by end-users.

### Content

The main content of Landing Pages constitute of Web Components. 
A dedicated page presents the list of available components.
The content can be placed in any of the three columns: Left, Center or Right. 
Based on where you will place your content, the columns will cover specific amount of the section. 

:::info
The percentage of page that each column will cover depends on which display you have chosen, e.g. centered content, fluid width etc..
:::

The following table shows the section coverage based on which columns you have chosen:

|Setting|Description|
|-------|-----------|
|**Only left column**|The content of the left column will cover 50% of the section.|
|**Only right column**|The content of the right column will cover 50% of the section.|
|**Only left and right column (without center column)**|The content of the left column will cover 50% of the section while the content of the right column will cover the rest 50% of  the section.|
|**Only center column**|The content of the center column will cover 100% of the section.|
|**Left & center column**|The content of the left column will cover 25% while the content of the center column will cover the rest 75% of the section.|
|**Center & right column**|The content of the center column will cover 75% of the section while the right column will cover the rest 25% of the section.|
|**Left, Center & right column**|The content of the left column will cover 25% of the  section, the content of the center column will cover 50% of the section  and the content of the right column will cover the rest 25% of the section|

## Web Components

The following web components are available:

### Hero Header
![Landing Page section wrappers.](/assets/img/2-cern-modules/hero_header.jpg)
<p className="image-caption">An example of a Hero Header.</p>

A hero component mostly used as the first component in order to introduce the reader to the topic of your page. You can add multiple frames with different backgrounds and text in order to look like a slideshow.

Usage Tips:

- Use it as the first component in your Landing Page.
- Use it in a section with either **Full Height** or **Half Height**.
- Use it with **Full Width**.
- Very useful to use it with **Has Header** setting since it looks nice when the main menu appears on top of the Hero Image.

<iframe scrolling="no"  src="https://videos.cern.ch/video/OPEN-VIDEO-2020-035-001" width="560" height="315" frameborder="0" allowfullscreen></iframe>

### Featured Banner
![Landing Page section wrappers.](/assets/img/2-cern-modules/featured_banner.jpg)
<p className="image-caption">An example of a Featured Banner.</p>

Informative component used in order to provide more information regarding a section. Each Featured Banner can have a background image, a link and a box with information.

Usage Tips:

- For better look n feel, use it in a section of **Fluid Width display**.
- You have the option to either use a CDS image or upload an image from your machine. 
- The box can be placed in 4 different positions: Top Left, Top Right, Bottom Left or Bottom Right.

<iframe scrolling="no"  src="https://videos.cern.ch/video/OPEN-VIDEO-2020-035-002" width="560" height="315" frameborder="0" allowfullscreen></iframe>

### Featured Quote
![Landing Page section wrappers.](/assets/img/2-cern-modules/featured_quote.jpg)
<p className="image-caption">An example of a Featured Quote.</p>

Presents a quote along with the name of the person who mentioned it.

Usage Tips:

- For better look n feel add it in a section of **Centered Content display**.

<iframe scrolling="no"  src="https://videos.cern.ch/video/OPEN-VIDEO-2020-035-003" width="560" height="315" frameborder="0" allowfullscreen></iframe>

### Image Gallery
![Landing Page section wrappers.](/assets/img/2-cern-modules/image_gallery.png)
<p className="image-caption">An example of an Image Gallery.</p>

A gallery component presenting images either local ones or from CDS. Each image has a download button for different sizes.

Usage Tips:

- You can add either local images (uploaded from your computer) or CDS images.
- CDS images pull fields automatically from CDS, including caption.
- Local images do not support caption.

<iframe scrolling="no"  src="https://videos.cern.ch/video/OPEN-VIDEO-2020-035-005" width="560" height="315" frameborder="0" allowfullscreen></iframe>

### Text Component
Adds formatted text in a section in order to write paragraphs.

Usage Tips:

- You can use Text Component with Lazy Section to make the text appear while scrolling.
- For a better experience you are advised to use the CERN Full HTML text format. The format provides out-of-the-box text styles.

<iframe scrolling="no"  src="https://videos.cern.ch/video/OPEN-VIDEO-2020-035-004" width="560" height="315" frameborder="0" allowfullscreen></iframe>

## Box Shapes

Boxes feature three types of displays:

- Portrait: The box has a vertical orientation. [red-bordered box in the image below]
- Simple: The box has a small parallelogram shape [green-bordered boxes in the image below]
- Landscape: The box has a horizontal orientation [blue-bordered boxes in the image below]

![The types of displays for boxes.](/assets/img/2-cern-modules/web_components_boxes.png)
<p className="image-caption">The types of displays for boxes.</p>

1. Display: Portrait [red box]
2. Display: Simple [green box]
3. Display: Simple [green box]
4. Display: Simple [green box]
5. Display: Simple [green box]
6. Display: Landscape [blue box]

In general, if you start the section with a portrait and then place four simple boxes one after the other, you will have the format with the red and green boxes as seen in the above screenshot.
Other layouts are also possible, e.g. three portraits in a row, or three simple boxes in a row.
