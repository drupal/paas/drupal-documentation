---
slug: /publications
---

# Talks & Presentations

| Item         | Date     | Link |
|--------------|-----------|------------|
| [SRECon 2023](https://www.usenix.org/conference/srecon23emea/presentation/barros) | Oct 2023 | [Slides](https://cds.cern.ch/record/2875365/files/presentation_print.pdf?version=1)
| [CHEP 2023](https://indico.jlab.org/event/459/contributions/11541/) | May 2023 | [Slides](https://indico.jlab.org/event/459/contributions/11541/attachments/9344/13882/Large%20scale%20dynamic%20web%20deployment%20for%20CERN%20experience%20and%20outlook.pdf)
| [KubeCon EU 2023](https://kccnceu2023.sched.com/event/1Hycm/operating-cern-saas-at-scale-with-operators-michael-hrivnak-varsha-prasad-narsing-red-hat-rajula-vineet-reddy-francisco-borges-aurindo-barros-cern) | April 2023 | [Slides](https://static.sched.com/hosted_files/kccnceu2023/d9/CERN_OperatorFrameowrk%20KubeConEU%2023.pptx.pdf), [Video recording](https://youtu.be/0sBgS_3xT8U)
| [CERN IT ASDF (Internal)](https://indico.cern.ch/event/1246355/) | January 2023 |  [Slides](https://indico.cern.ch/event/1246355/contributions/5236764/attachments/2582678/4454836/ASDF%20Post-mortem%20OTG0074884%20-%20Drupal%20infrastructure%20-%20Unavailability%20of%20sites%20-%20CodiMD.pdf)
| [DrupalCon EU 2022](https://events.drupal.org/prague2022/sessions/managing-fleet-custom-drupal-sites-centrally-cern) | September 2022 | [Video recording](https://www.youtube.com/watch?v=uIp_BYTsHq4)
| [CERN IT ASDF (Internal)](https://indico.cern.ch/event/1185867/) | July 2022 |  [Slides](https://indico.cern.ch/event/1185867/contributions/4982565/attachments/2486934/4270235/home.cern%20vs%20'LHC%20Run3,%20Pentaquarks,%20Fake%20news'.pdf)
| [Cloud Native Rejekts EU (Valencia) 2022](https://cfp.cloud-native.rejekts.io/cloud-native-rejekts-eu-valencia-2022/talk/CLKZ3U/) | May 2022 |  [Video recodring](https://www.youtube.com/watch?v=Axp2yituyAk)
| [CERN IT ASDF (Internal)](https://indico.cern.ch/event/1122757/) | March 2022 |  [Slides](https://indico.cern.ch/event/1122757/contributions/4713456/attachments/2401506/4107305/ASDF_%20Migration%20to%20Drupal%20Cloud.pdf)
| [HEPiX Autumn 2021](https://indico.cern.ch/event/1078853/contributions/4580656/) | October 2021 | [Slides](https://indico.cern.ch/event/1078853/contributions/4580656/attachments/2334251/3978455/HEPiX%20Survival%20kit%20for%20CERN's%20organic%20webscape%20Kubernetes%20and%20a%20community%20of%20Makers.pdf)
| [DrupalCon EU 2021 Talk 1](https://events.drupal.org/europe2021/sessions/survival-kit-cerns-organic-webscape-kubernetes-and-community-makers) | October 2021 | [Video recording](https://www.youtube.com/watch?v=H_27mPG-0VM)
| [DrupalCon EU 2021 Talk 2](https://events.drupal.org/europe2021/sessions/building-cloud-native-saas-cerns-1k-drupal-sites) | October 2021 | [Video recording](https://www.youtube.com/watch?v=be96lcb1B-Y)
| [vCHEP 2021](https://indico.cern.ch/event/948465/) | August 2021 |  [Paper](https://www.epj-conferences.org/articles/epjconf/abs/2021/05/epjconf_chep2021_02064/epjconf_chep2021_02064.html)
| [CERN IT ASDF (Internal)](https://indico.cern.ch/event/1050714/) | July 2021 |  [Slides](https://indico.cern.ch/event/1050714/contributions/4418445/attachments/2282721/3878784/ASDF%20-%20Drupal%20SaaS%20on%20Kubernetes_%20a%20case%20for%20cross-team%20collaboration.pdf)
| [CERN IT White Area/ Terra Incognita](https://indico.cern.ch/event/1027970/) | May 2021  | [Slides](https://indico.cern.ch/event/1027970/attachments/2232600/3783392/White%20Area%20Sailing%20with%20Operators.pdf), [Video recording](https://videos.cern.ch/record/2765238)
| [KubeCon EU 2021](https://kccnceu2021.sched.com/event/iE36/cerns-1500-drupal-websites-on-kubernetes-sailing-with-operators-konstantinos-samaras-tsakiris-rajula-vineet-reddy-cern?iframe=no&w=100%&sidebar=yes&bg=no) | March 2021 | [Slides](https://static.sched.com/hosted_files/kccnceu2021/f0/CERN%27s%201500%20Drupal%20websites%20on%20k8s%20--%20Sailing%20with%20Operators.pdf), [Video recording](https://www.youtube.com/watch?v=4O-pcSQR8Vw)
