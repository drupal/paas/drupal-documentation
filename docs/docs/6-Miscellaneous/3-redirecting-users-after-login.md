---
slug: /miscellaneous/redirection
---

# Redirect After Login

:::info
This functionality is included by default in the CERN Drupal Distribution.
:::

If your website features content that requires users to login, it is often convenient to redirect them to the page they were originally attempting to access after successfully authenticating.
Indeed, the default behaviour is to direct users to their dedicated `https://my-website.web.cern.ch/user/id` page, often causing confusion.
However, it is very easy to ensure that users are redirected to the page they attempted to access in the first place using the `r4032login` module (https://www.drupal.org/project/r4032login).

0. Head to the **Extend** tab and ensure `r4032login` is installed and enabled. Search for "r403".

If it is not enabled, simply tick it and click **Install**. This needs to be done before proceeding.

![The r4032login module under Extend.](/assets/img/6-miscellaneous/redirect_1.png)

<p className="image-caption-shadow">The r4032login module under Extend.</p>

1. With `r4032login` installed and enabled, we need to configure it.

In the **Extend** tab, search for "r403" and click the module. Click **Configure**.

![Configure the r4032login module under Extend.](/assets/img/6-miscellaneous/redirect_2.png)

<p className="image-caption-shadow">Configure the r4032login module under Extend.</p>

2. On the configuration page, scroll down to the **REDIRECT 403 TO USER LOGIN** section.

Verify that the Redirect user to the page they tried to access after login option is **ticked**. If it is not, tick it.

Verify that the Display access denied message on the login page option is **unticked**. If it is not, untick it.

![Configure the r4032login module under Extend.](/assets/img/6-miscellaneous/redirect_3.png)

<p className="image-caption-shadow">Configure the r4032login module under Extend.</p>

3. Once done, scroll to the bottom and click **Save configuration**.

![Configure the r4032login module under Extend.](/assets/img/6-miscellaneous/redirect_4.png)

<p className="image-caption-shadow">Configure the r4032login module under Extend.</p>

![Configure the r4032login module under Extend.](/assets/img/6-miscellaneous/redirect_5.png)

<p className="image-caption-shadow">Configure the r4032login module under Extend.</p>

Users will now be redirected to the page they tried to access after logging in.
