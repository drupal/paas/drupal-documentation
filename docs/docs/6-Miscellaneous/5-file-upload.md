---
slug: /miscellaneous/forms-file-upload
---

#  File Upload in Forms

:::info
The default site storage size is 2GB. 
For larger websites, storage is automatically incremented as they grow. 
The limit is 50GB.
If you exceed this, please open a ticket.
:::

Drupal sites storage is intended to be used for website serving purposes. 
It is meant to include **only** the files needed to deploy and serve the website.
Accordingly, for websites which handle webform submissions, external storage should be used. 
Using the internal storage leads to problems with operations, such as backups, cloning, updates, and upgrades.

### Recommendations

:::info
File upload in Forms is limited to 60MB. For larger files, one should host them elsewhere such as CDS or CERNBox)
:::

Instead of allowing the form users to submit a file directly, one should accept an URL to a file (CERNBox, Dropbox, Google Drive, Microsoft OneDrive, etc..).

The recommended file storage at CERN in CERNBox.

To avoid permission issues we recommend you add a mention regarding file access, requesting that users of the form should give read/download access to the link. 
In CERNBox, this is done by coppying the link via the `Share publicly` option.

![Webform file upload example](/assets/img/6-miscellaneous/file_upload.png)
