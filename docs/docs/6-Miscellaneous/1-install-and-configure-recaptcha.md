---
slug: /miscellaneous/recaptcha
---

# Install reCAPTCHA

Whenever a contact or submission form is publicly available, it is good practice to secure it.

This way, spam submissions are significantly reduced.
This not only eases the process of managing submissions, it also helps secure your website from potential vulnerabilities otherwise exploitable through input forms.
A number of community contributed modules are available, and careful consideration must be paid to how they impact the user experience.
Google's **reCAPTCHA** is generally regarded as being highly effective at stopping spam whilst being easy on humans.
Accessibility support is, moreover, included, thereby helping ensure as many as possible can interact with your website without issues.

Kindly note that this guide assumes the usage of **Webforms**.

## Install reCAPTCHA

:::info
reCAPTCHA is included in the CERN Drupal Distribution by default.
**Only** install manually if your website differs.
:::

1. Head to https://www.drupal.org/project/recaptcha and select the newest release supporting your version of Drupal.

![The reCAPTCHA module homepage.](/assets/img/5-development/recaptcha_1.png)

<p className="image-caption-shadow">The reCAPTCHA module homepage.</p>

In this case, we proceed with version **8.x-3.0**.
Always ensure to download the **newest available version**.

Download and unzip the **.zip** and install it on your website.

If you are unsure how to update and install modules, please see this guide for more information.

## Configure reCAPTCHA

2. Once installed, a new setting becomes available under **Configuration**.

Head to `https://your-website.web.cern.ch/admin/config` and select **CAPTCHA module settings**.

![The reCAPTCHA module settings.](/assets/img/5-development/recaptcha_2.png)

<p className="image-caption-shadow">The reCAPTCHA module settings.</p>

3. Head to the dedicated reCAPTCHA tab at the top to finalise the installation.
   The following needs to be done only once.

![The reCAPTCHA module settings.](/assets/img/5-development/recaptcha_3.png)

<p className="image-caption-shadow">The reCAPTCHA module settings.</p>

We require both a **Site key** and a **Secret key** in order to activate reCAPTCHA.

![The reCAPTCHA module homepage.](/assets/img/5-development/recaptcha_4.png)

<p className="image-caption-shadow">The reCAPTCHA module homepage.</p>

4. Click **register for reCAPTCHA**, or head directly to https://www.google.com/recaptcha/admin/create.

Kindly note that this step requires a Google Account. If you do not already have an account, please create one.

:::note
If you are creating anything but a personal website, use a dedicated e-mail for this such that others may assume development in the future.
:::

![Registering a new website for reCAPTCHA.](/assets/img/5-development/recaptcha_5.png)

<p className="image-caption-shadow">Registering a new website for reCAPTCHA.</p>

5. Fill out the details, accept the Terms of Services and click **Submit**.

Choose whichever **reCAPTCHA type** and associated validation process is preferred.

The most common is **reCAPTCHA v2** coupled with the **"I'm not a robot" Checkbox** which presents a challenge to the user.

![Choosing a reCAPTCHA type.](/assets/img/5-development/recaptcha_6.png)

<p className="image-caption-shadow">Choosing a reCAPTCHA type.</p>

Submit the form. This will load the below page.

![Generated reCAPTCHA keys.](/assets/img/5-development/recaptcha_7.png)

<p className="image-caption-shadow">Generated reCAPTCHA keys.</p>

6. Copy-paste the newly generated **Site key** and **Secret key**. Never share these!

Depending on use-case, the **Local domain name validation** and **Use reCAPTCHA globally** options might be considered.

Kindly note that if a website uses a `.cern` domain, the _Local domain name validation_ should not be ticked!

![Finished reCAPTCHA configuration.](/assets/img/5-development/recaptcha_8.png)

<p className="image-caption-shadow">Finished reCAPTCHA configuration.</p>

Click **Save configuration**.

7. Return to the **CAPTCHA Settings** main tab.

Select **reCAPTCHA (from module recaptcha)** if this is not already selected under **FORM PROTECTION**.

Tick the **Add CAPTCHA administration** links to forms options.

![Configuring form protection using reCAPTCHA.](/assets/img/5-development/recaptcha_9.png)

<p className="image-caption-shadow">Configuring form protection using reCAPTCHA.</p>

Other settings on this page are unrelated to the configuration of reCAPTCHA, though may be relevant to confirm for your form.

Click **Save configuration**.

## Adding a CAPTCHA

8. We are now ready to add reCAPTCHA to our form.

Head to **Structure** and choose Webforms.

![Managing Webforms.](/assets/img/5-development/recaptcha_10.png)

<p className="image-caption-shadow">Managing Webforms.</p>

9. Click **Build** on the form of interest.

![Building a webform.](/assets/img/5-development/recaptcha_11.png)

<p className="image-caption-shadow">Building a webform.</p>

10. Click **Edit** on the **[captcha]** entry.

![Managing Webforms.](/assets/img/5-development/recaptcha_12.png)

<p className="image-caption-shadow">Managing Webforms.</p>

Select **reCAPTCHA (from module recaptcha)** under **Challenge type**.

11. Click **Save**. The side panel closes.

12. Click **Save elements**.

A reCAPTCHA box will now be shown on the webform.
