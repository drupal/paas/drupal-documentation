---
slug: /miscellaneous/jquery-for-drupal-9
---

# jQuery for Drupal 9

:::warning
Alert This guide is for advanced users only. 
If you are not comfortable working through the terminal, editing code, or overriding default options, please do not proceed. 
If you are unsure whether you require the functionality outlined in this guide, please reach out via the community forums. 
There may be an easier way!
:::

As part of Drupal 9.0, most jQuery UI asset libraries were deprecated and moved to contributed modules only. 
This means that while jQuery UI (see https://openjsf.org/projects/#emeritus and https://jqueryui.com/ for more information) was previously included as part of the Drupal Core directly, it and any sub-modules must now be installed independently by the website owner. 
This change recognises the fact that while jQuery UI was originally introduced in 2009, it ultimately has not been actively maintained since 2017. 

**We thus strongly recommend finding an alternative for the longer term!**

Since a number of CERN websites continue to rely on jQuery UI, the CERN Drupal Distribution includes

```yaml
"drupal/jquery_ui": "~1.4.0"
"drupal/jquery_ui_accordion": "~1.1.0"
"drupal/jquery_ui_datepicker": "~1.2.0"
"drupal/jquery_ui_draggable": "~1.2.0"
"drupal/jquery_ui_droppable": "~1.2.0"
```

by default.

Additional jQuery UI modules may be found here: https://www.drupal.org/project/jquery_ui.

## Updating a Module

The following outlines the steps needed to replace `core/jquery.ui.accordion` in a module.

1. Download and install the [jQuery UI Accordion](https://www.drupal.org/project/jquery_ui_accordion) module.

2. Add a dependency to the module's `info.yml` file.

```yaml
dependencies: 
    - jquery_ui_accordion:jquery_ui_accordion 
```

3. Change any reference for `core/jquery.ui.accordion` to `jquery_ui_accordion/accordion`

Repeat these steps for any other sub-dependencies.

## Updating a Theme

The following outlines the steps needed to replace `core/jquery.ui.accordion` in a theme:

1. Download the [jQuery UI Accordion](https://www.drupal.org/project/jquery_ui_accordion) module.

2. Also check if there's a libraries-extend entry for this asset library:

```yaml
libraries-extend:
  core/jquery.ui.accordion:
    - mytheme/mytheme.jquery.ui.accordion
```

3. Copy/paste this, and change `core/jquery.ui.accordion` to `jquery_ui_accordion/accordion`, like so:

```yaml
libraries-extend:
  jquery_ui_accordion/accordion
    - mytheme/mytheme.jquery.ui.accordion
```

4. Also check if there's a `libraries-override` entry for this asset library:

```yaml
libraries-override:
  core/jquery.ui.accordion:
    css:
      theme:
        assets/vendor/jquery.ui/themes/base/accordion.css: false

```

5. Copy/paste this, and change `core/jquery.ui.accordion` to `jquery_ui_accordion/accordion` and look up in the downloaded contrib module's `*.libraries.yml` file what the updated path is, like so:

```yaml
libraries-override:
  jquery_ui_accordion/accordion
    css:
      theme:
        jquery.ui/themes/base/accordion.css: false
```

6. Only the installed asset libraries (the core or contributed one, or both in a transitional phase) will be overridden or extended!

Repeat these steps for any other sub-dependencies.
