---
slug: /roadmap-wordpress
---

# Roadmap to WordPress

We are excited to share our roadmap for WordPress at CERN.

WordPress presents a unique opportunity to modernise our extensive digital presence, leveraging the rich WordPress ecosystem to address the many shortcomings and difficulties inherent to Drupal, such as accessibility, responsiveness, and workflow efficiency.
Our goal is to simplify the web development process for website owners, allowing them to focus on what truly matters: creating and managing their content, without the complexities associated with Drupal.

Our roadmap is as follows:

<div id="gantt-chart-container"></div>

* **WordPress Lite** (October 2024): A lightweight version of WordPress offering an initial set of plugins and the first version of our official CERN WordPress theme running on the existing `app-catalogue` infrastructure. This offering will provide a simple and accessible way for website owners to familiarise themselves with WordPress.
* _**WordPress MVP** (December 2024): An initial, internal version of the dedicated WordPress infrastructure. Available only to the Web Team for development and testing purposes._
* _**WordPress Pilot** (March 2025): An internal pilot of the dedicated WordPress infrastructure. Available only to the Web Team and select websites for development and testing purposes._
* **WordPress** (May 2025): WordPress becomes generally available to everyone at CERN.
* **Migration** (July 2025): We begin automated migrations from Drupal to WordPress.

## FAQ

**Q1: Who can access _WordPress Lite_?**

_Everyone. However, due to WordPress Lite being restricted and subject to numerous updates throughout its lifetime, .cern websites are not allowed and we generally discourage high-traffic websites from moving to WordPress Lite._

**Q2: What happens once _WordPress Lite_ closes?**

_Websites created on WordPress Lite will automatically be migrated to the official, dedicated WordPress infrastructure once available. No action will be necessary. We will share more information available about this once we are closer to the official release._

**Q3: Can I move my Drupal website to _WordPress Lite_?**

_While we will be migrating Drupal websites to WordPress automatically, we will not be offering this as part of WordPress Lite. As such, if you wish to shift your Drupal website to WordPress today, you are able to do so, but you have to manually migrate your content._

**Q4: Can I install `x` plugin on _WordPress Lite_?**

_No. WordPress Lite will come with a limited set of plugins that may expand during its lifetime. You will not be able to install your own plugins or themes. If you have requirements or suggestions, please get in touch and we may be able to include it in a future update._

**Q5: What is _WordPress MVP_ and _WordPress Pilot_?**

_These are internal versions of WordPress running on a new, dedicated WordPress infrastructure. It will not be possible to create a website on these versions as only the Web Team will be using them for development and testing purposes. Work includes refining the CERN WordPress Distribution, automated end-to-end testing, automated migration, and integration with various CERN services. The output of this work will conclude with the general release of WordPress._

**Q6: What happens to Drupal and my Drupal website(s)?**

_All Drupal websites which require a content management system will be migrated to WordPress. No website will be served on WordPress before it has been verified and approved. Drupal itself will continue to be supported and maintained throughout 2024 and 2025. However, Drupal will eventually be decommissioned as Drupal 10 reaches end-of-life in Q3/Q4 2026._

**Q7: I need a new website. Where should I create it today?**

_Firstly, only websites that require a content management system (CMS) should evaluate WordPress or Drupal. You can see an overview of other options available at [https://webservices-portal.web.cern.ch/](https://webservices-portal.web.cern.ch/)._

_If indeed a CMS is needed, the general advice depends on a couple of factors:_

- 1) _If you are already familiar with Drupal, a new website can be created in Drupal. Since customisation is already blocked for new websites, once time comes, your website will be migrated automatically into WordPress. In the meantime, you will benefit from all the perks a centrally managed infrastructure offers._
- 2) _If you are not familiar with Drupal, or prefer not to continue with Drupal, then the advice is to wait to start development until WordPress Lite is available._
    
**Q8: I have a website on Drupal. What should I be doing to prepare for the migration?**

_Websites confirming to the CERN Drupal Distribution will be migrated automatically to WordPress. The best you can do to prepare is strip away non-essential customisation._

**Q9: I already have a website on WordPress in app-catalogue. What will happen to it? Should I be doing anything?**

_Existing WordPress websites will co-exist with WordPress Lite. You will benefit from everything WordPress Lite offers, but we will not overwrite existing customisation._

_Similarly to Drupal, it's your responsibility to remove existing customisation in order to conform with WordPress Lite. As soon as Lite in released (October 2024), you should adapt your website to use only the plugins centrally provided._

_Once central WordPress is available (May 2025), WordPress Lite websites must migrate. If your website conforms, we will handle it automatically without any action needed._
_WordPress Lite is not the final solution and will be decommissioned after May 2025._

**Q10: I have a question not answered here?**

_Please contact [wordpress-feedback@cern.ch](mailto:wordpress-feedback@cern.ch) and we will get back to you._