---
slug: /deleting-websites
---

# Deleting Websites

Deleting Drupal websites is done via the [Webservices Portal](https://webservices-portal.web.cern.ch/).

:::note
Webservices Portal has replaced [https://webservices.web.cern.ch/webservices/](https://webservices.web.cern.ch/webservices/).
The new portal is actively being developed and will be subject to a number of updates throughout 2022 and 2023.
If you created your website on the old portal, it will also be accessible via the new portal.
If you encounter any issues using the new portal, or would like to request a new feature, please [submit a ticket](https://cern.service-now.com/service-portal?id=service_element&name=web-portal).
:::

Anyone with a CERN account and administrative access to a specific website may delete it.

1. Open your browser and head to https://webservices-portal.web.cern.ch/. Sign in.

2. Click **My Sites**.

![The Webservices Portal frontpage.](/assets/img/1-getting-started/clone_website_1.png)
<p className="image-caption-shadow">The Webservices Portal frontpage.</p>

3. Search for the website you wish to delete. Click the cog.

![Searching for a website on the Webservices Portal.](/assets/img/1-getting-started/clone_website_2.png)
<p className="image-caption-shadow">Searching for a website on the Webservices Portal.</p>

4. This page shows you ownership and other general information about your website.

In order to delete a website, select the specific website you wish to delete from the menu.

![Deleting a website on the Webservices Portal.](/assets/img/1-getting-started/delete_website_1.png)
<p className="image-caption-shadow">Deleting a website on the Webservices Portal.</p>

:::danger
Do not proceed unless you are absolutely certain the website in question is okay to delete!
:::

Click **Delete this environment**.

![Deleting a website on the Webservices Portal.](/assets/img/1-getting-started/delete_website_2.png)
<p className="image-caption-shadow">Deleting a website on the Webservices Portal.</p>

Your website is now deleted.
