---
slug: /creating-content
---

# Creating Content

Once you have created your Drupal website, you are ready to add content.

All content creation for Drupal websites is done directly via the browser by accessing your website and logging in.
Once logged in, an administrative menu will be shown at the top.
Anyone with a CERN account may login on any CERN website, but only those with relevant permissions (typically managed via grappa or e-groups) can make changes to the website.
Depending on how a specific website is configured, distinct roles (e.g. writer, editor, etc.) with different permissions may be implemented.
This allows owners to micromanage exactly which functionality should be accessible to which users.

:::note
Additional information on Roles and Permissions is available [here](https://drupal.docs.cern.ch/docs/Getting%20Started/roles-and-permissions).
:::

Once logged in, click the **Content** tab in the upper-left corner.

![An authenticated user looking at the Content interface.](/assets/img/1-getting-started/creating_content_1.png)

<p className="image-caption-shadow">An authenticated user looking at the Content interface.</p>

The **Content** interface shows content created on your website.

Adding new content is done via the **Add content** button.

![Adding new content to a website.](/assets/img/1-getting-started/creating_content_2.png)

<p className="image-caption-shadow">Adding new content to a website.</p>

On this page, you choose the type of content you wish to add.

Drupal supports a number of different content types, e.g. _Article_ or _Basic page_.
Different types of content offer different types of features.
As such, choosing one content type above another typically depends on what exactly you wish to create.
Installing community contributed modules typically introduces new content types that can be freely used.

:::note
While the content types included by default generally accommodate most use-cases, it is possible to

- (a) install and enable additional modules; and
- (b) create your own content types if you have very specific requirements.

Information on how to create your own content type(s) is available [here](https://www.drupal.org/docs/user_guide/en/structure-content-type.html).
:::

In this example, we will proceed with an **Article**.

![Adding new content to a website.](/assets/img/1-getting-started/creating_content_3.png)

<p className="image-caption-shadow">Adding new content to a website.</p>

The **Title** specifies the title of your content.
Please avoid very long titles to better accommodate different screen sizes!
The **Body** is a complete _WYSIWYG (What You See Is What You Get)_-editor with traditional text formatting features known from LibreOffice or Word.
Specifically, Drupal 9 uses the CKEditor version 4.0, whereas Drupal 10 uses CKEditor version 5.0 (read more [here](https://ckeditor.com/ckeditor-5/)).
**Tags** may be added to aid searches on your website, but are not mandatory.
The entire interface shown in the above screenshot is also referred to as the CKEditor in Drupal terminology.
Kindly note that different types of content have different types of fields which need to be filled out or checked.
In the case of articles, the only mandatory setting is deciding whether to allow comments or not (note the \* on the right-hand side).

Once you are satisfied with your content, click **Publish**.

Once content has been published, it is generally publicly accessible.
