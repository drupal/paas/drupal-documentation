---
slug: /
---

# Drupal

Welcome to **drupal.docs.cern.ch**, your one-stop Drupal resource at CERN.

## News

### September 2024: Update to timeline and plans, WordPress Lite and much more

Please check the dedicated section to the [Roadmap to WordPress](/roadmap-wordpress) to see the updates to the plans and timelines.

We'll go over these details in the [Community Meeting](https://indico.cern.ch/event/1457632/) of 26th September via zoom. 


### April 2024: The road from Drupal to WordPress 🎉

We write this news to share that the presentation at the Enlarged Directorate, on March 5th, on the topic of _“Web presence, Governance and Drupal to WordPress migration”_ was very well received.
**A decision to move from Drupal to WordPress, followed by decommissioning Drupal at CERN, has been approved.** 
Given the additional resources required for this project, in order to develop the infrastructure in-house and support the migration for all CERN Websites, new resources will be requested (Q3/Q4 onwards).
However, work is already starting on the preparations of the new infrastructure and the subsequent migration to WordPress.
A strategy for customised websites is still being defined. 
It will be shared as soon as possible.
If you have feedback or concerns at this stage, please contact your Web Representative directly.

Some key takeaways to share:
- Drupal websites which adhere to existing recommendations and thus limit their usage to the `CERN Drupal Distribution` will be offered an automated migration path.
Drupal users are asked to wait for concrete plans for an automated migration to WordPress and to refrain from attempting a migration on their own. 
We cannot offer support to such instances.

- The current version of WordPress on the `app-catalogue`, see [wordpress.docs.cern.ch](https://wordpress.docs.cern.ch), is _not_ going to be the final, centrally-managed offering.
Changes to the current offering are thus to be expected (including functionally, infrastructure, processes, and look-and-feel).
Websites on the current self-managed WordPress infrastructure will be moved to the central one, once available, in an effort to consolidate and offer the same level of support to all WordPress users.
It is not an option to remain on the `app-catalogue` infrastructure indefinitely.
We will be contacting all impacted websites once a migration path is available.

- The final WordPress offering of plugins cannot be shared at this date. 
A dedicated CERN WordPress theme will be provided, as well as a CERN WordPress Distribution of curated and customised plugins.

- All new customisation is therefore, and from now on, blocked on **new** Drupal websites. 
This comes aligned with the decision to move to WordPress in an uniform way. 
Work is ongoing to review the custom module utilisation and support the most used ones centrally.
However, preliminary analysis shows a wide-ranging distribution of custom module usage (more than 300 unique modules over 200 customised websites).
** Therefore, we recommended all Drupal users to assess and uninstall non-essential custom modules from their websites, in order to prepare for the upcoming move to WordPress.**

- Given that the current offering on WordPress comes with limited support, we recommend experienced Drupal users to keep using Drupal, if there’s a need for a new website.

- The preliminary plan is to freeze the Drupal infrastructure as much as possible, in order to save resources for the upcoming migration (both for users and ourselves).

Thank you for all the input and support!

## Roadmap

### Drupal Infrastructure 
The Drupal service follows as close as possible the Drupal release cycle and support at most two versions at the same time.
Once a Drupal release becomes end-of-life (EOL), it will no longer be supported at CERN as well.
Accordingly, users must ensure their website is running the supported Drupal version to receive centralised support.

All updates and upgrades are communicated to the users via the [Notifications service](cern.ch/n-91d) and/or via a [Service Status Board](https://cern.service-now.com/service-portal?id=service_status_board) announcement. 

**The only supported version currently is Drupal 10.** Drupal 10 is expected to be supported at CERN until February of 2026. Drupal 10 is expected to be the last release offered at CERN.

Drupal 9 is EOL since November 2023.

### Drupal to WordPress

See the updated [roadmap](/roadmap).

** All Drupal users are recommended to uninstall non-essential custom modules from their websites, in order to prepare for the upcoming move to WordPress.**

Due to this change, the Drupal infrastructure is in maintenance mode, in order to save resources for the upcoming migration (both for users and ourselves), therefore no active development is planned.

## Service level

Drupal is a service centrally provided by the IT department in close collaboration with the Web Team (IR-ECO).

:::info Website development
No expertise or support is provided regarding website development. 
**Support is limited to the infrastructure maintenance or website malfunction.**
It is thus strongly recommended to not exceed what is offered centrally in the CERN Drupal Distribution as this introcue additional complexity and maintenance which would be the responsibility of the website owner(s).
For guidance regarding Drupal development or theming, please consider reaching other community experts via the means listed under [#Support](#support).
:::

### User responsibility
A Drupal website is by default publicly accessible.
Accordingly, as with any IT offering, it must adhere to CERN's rules, regulations and guidelines on subjects such as design, accessibility, and security. 
Additionally, in order to ensure CERN's Digital Identity, any website must adhere to the CERN Web Presence Guidelines. 

In this way, all users must adhere to all guidelines indicated thought out the documentation or risk having their website blocked without prior notice. Additionally, make sure you follow all the guidelines under [Data Privacy](#data-privacy).
This is particularly important if additional, custom modules are installed which collect or otherwise aggregate user data.

#### CERN Drupal Distribution and customisation

CERN provides and maintains a curated collection of Drupal modules called the CERN Drupal Distribution. 
For the full list of modules, please refer to [Introduction to Modules](/modules).
Any module or functionality which exceeds what is centrally provided is the sole responsibility of the website owner and its administrators.

**The official recommendaiton is to avoid installing any additional module(s) or theme(s)!**
Conforming exclusively to the central distribution is the only way to ensure smooth operations, and to benefit from centrally provided upgrades and migrations.
If you inherit a customised website, the general recommendation is to attempt simplifying it, stripping away any customisation and ensuring conformance with what is centrally offered.
If you find yourself in this situation, please do not hesitate reaching out for assistance.

:::warning Customisation disclaimer
Please note that installing and using custom modules requires:
- (a) continuous maintenance; 
- (b) continuous attention to updates, security patches, and new feature releases;
- (c) ensuring compatibility with the supported Drupal version; and
- (d) manual work for the webmaster who will have to carefully check that everything works as expected on their side everytime any central upgrade is available.

**This work will solely be the responsibility of the website owner(s) as any custom module not in the CERN Drupal Distribution will not benefit from the centrally managed updates.** 

Any website is a living and breathing piece of software.
As such, once a given Drupal version reaches end-of-life, it will no longer receive further security updates.
Running an outdated version of Drupal necessarily puts the website, and consequently CERN's reputation, at risk.
Accordingly, upgrades will, at some point, be forced through; even if doing so breaks compatibility with customised modules.

If you require a set-and-forget solution, please do not exceed what is offered in the CERN Drupal Distribution.
By using this service with custom modules, you agree to take active part in the upgrades process, allowing us to follow the upstream release cycles in due time.
:::

In the event that the modules provided via the CERN Drupal Distribution are not sufficient for your needs, we recommend you either:
- post on the CERN Drupal Community Forums/Mattermost to see whether others have already achieved what you are looking for; 
- submit a request to the Drupal Team to evaluate whether the feature(s) you require can be accommodated by adding a new module to the CERN Drupal Distribution, benefiting all CERN websites.

## Data privacy

Please refer to the information in the CERN's Data Privacy Office website and make sure that you read and agree with the [Drupal Service Data Privacy Police](https://cern.service-now.com/service-portal?id=privacy_policy&se=Drupal-Service&notice=drupal-service) before using the service. 

## Support

We strive to make these docs your go-to place for anything Drupal.
We are continuously expanding the website with guides and tutorials for most use-cases.

There are various ways to get in touch, depending on your needs:

- Mattermost channel [Drupal Infra](https://mattermost.web.cern.ch/it-dep/channels/drupal) is a place for informal, technical exchange with the community and with the service managers.
- [Drupal Community Forum](https://drupal-community.web.cern.ch) is a place for informal, technical exchange with advanced users from the community.
- For any questions or concerns regarding the service or your website, please open a [ticket](https://cern.service-now.com/service-portal?id=service_element&name=Drupal-Service).


## How can I help?

This documentation is open and accessible by everyone with a CERN account.
If you spot an error, a typo, or would like to propose changes, please click _Edit this page_ at the bottom of any page.
Clicking this link takes you to the below screen directly on our Gitlab page (https://gitlab.cern.ch/drupal/paas/drupal-documentation/).
On here, you can submit your proposed changes.
We will then review your submissions and update the documentation accordingly.

We intend for this documentation to be living and breathing.
