---
slug: /cern-infrastructure
---

# CERN Infrastructure

As of November 2021, all CERN websites are served by the OpenShift cloud infrastructure.

This means that everything infrastructure related is handled by the **IT Drupal Infrastructure team** whereas everything related to official modules or themes is handled by the **Web Team**.
Similarly, as every website now runs on the same underlying infrastructure, we centrally manage updates to the Drupal Core and CERN-official modules and themes via the [CERN Drupal Distribution](https://gitlab.cern.ch/drupal/paas/cern-drupal-distribution).
This, in practice, means that website owners need not worry about receiving nor applying security updates as this is automatically handled.

:::warning
Unless specific functionality or customisation is desired, no action is necessary to ensure that your website and modules remain up-to-date.
Any customised or locally installed modules (i.e. modules _not_ included in the central distribution) _are solely the responsibility of the individual website owner_.
As updates to the Drupal Core (e.g. new functionality or security patches) are applied centrally, it is important to ensure continued compatibility with any locally installed modules.
:::

The below screenshot shows `my-website.web.cern.ch` running in the infrastructure.

![The OpenShift OKD interface showing my-website.web.cern.ch.](/assets/img/1-getting-started/cern_infrastructure_1.png)

<p className="image-caption-shadow">The OpenShift OKD interface showing my-website.web.cern.ch.</p>

:::note
The CERN Drupal infrastructure may be accessed via https://drupal.cern.ch/.
If you cannot find your project (i.e. website), please ensure that you are a member of the associated e-group.
If you have confirmed this to be the case, and still cannot see your resources, please get in touch.
:::

### CERN and Drupal Develoment

In short, **CERN is not involved in the development of Drupal**.

Indeed, work on Drupal itself is coordinated and completed by the [Drupal Association](https://www.drupal.org/association/about) and the many independent developers comprising the wider Drupal Community.
CERN thus has no say in when (or even if) updates are released, nor what is included in a given update.
Instead, the CERN IT Drupal Infrastructure Team and Web Team work in close collaboration to update the CERN Drupal Distribution following changes to Drupal.
The below graph illustrates the flow of updates between CERN, the Drupal Association, and the CERN Drupal Distribution.

![An overview of the flow of updates between CERN and the Drupal Association.](/assets/img/1-getting-started/cern_infrastructure_2.png)

<p className="image-caption-shadow">An overview of the flow of updates between CERN and the Drupal Association.</p>

As the Web Team is further responsible for all CERN-official modules and themes, continuous work is also being conducted to ensure compatibility with each core update.
This means that website owners across CERN need not worry about updates or compatibility maintenance.
However, again, any website owner with customised or otherwise locally installed modules must assume responsibility for these to ensure continued compatibility.
