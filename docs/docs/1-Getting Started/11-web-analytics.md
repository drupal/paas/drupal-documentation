---
slug: /web-analytics
---

# Web Analytics

Registering a Drupal website in Matomo via the [Web Services Portal](https://webservices-portal.web.cern.ch/).

1. Open your browser and head to https://webservices-portal.web.cern.ch/. Sign in.

2. Click **My Sites**.

![The Web Services Portal landing page.](/assets/img/1-getting-started/web_analytics_1.png)

<p className="image-caption-shadow">The Web Services Portal landing page.</p>

3. Search for the website you wish to register in Matomo. Click the cog.

![Searching for a website on the Web Services Portal.](/assets/img/1-getting-started/web_analytics_2.png)

<p className="image-caption-shadow">Searching for a website on the Web Services Portal.</p>

4. Under **Environments**, select the website you want to register in Matomo.

![Selecting website to register in Matomo.](/assets/img/1-getting-started/web_analytics_3.png)

<p className="image-caption-shadow">Selecting website to register in Matomo.</p>

5. Click on **Web Analytics** tab and then click on **Register site for Matomo** button.

![Registering website in Matomo.](/assets/img/1-getting-started/web_analytics_4.png)

<p className="image-caption-shadow">Registering website in Matomo.</p>

7. Click on **Drupal configuration page** to configure Matomo Analytics module.

![Message after registering the website in Matomo.](/assets/img/1-getting-started/web_analytics_5.png)

<p className="image-caption-shadow">Message after registering the website in Matomo.</p>

8. Set the **Matomo site ID** and click on **Save configuration**.

![Configuring Matomo Analytics module in website.](/assets/img/1-getting-started/web_analytics_6.png)

<p className="image-caption-shadow">Configuring Matomo Analytics module in website.</p>

:::note
If you have registered your site in the past for Piwik, please [submit a ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&fe=drupal-infrastructure) by providing the **site URL** and the **Matomo site ID** and we will configure the Matomo Analytics module for you.
:::

9. In the **Web Analytics** tab you have an overview of the visits of your website.

![Web Analytics overview.](/assets/img/1-getting-started/web_analytics_7.png)

<p className="image-caption-shadow">Web Analytics overview.</p>

Otherwise, click on **View Matomo Analytics** for more statistics.
