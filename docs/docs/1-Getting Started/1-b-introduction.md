---
slug: /introduction
---


# Introduction

If you have no idea what Drupal is, please read on!

### What is Drupal?

Drupal is a content-management framework with a high degree of customisation and flexibility.

A content-management framework, or, more commonly, content-management system (CMS), is software which helps users create, manage, and modify websites and content.
Drupal, in all its simplicity, does just this.
Thanks to a comprehensive suite of **modules, themes, and other packages, Drupal is highly flexible and offers almost endless customisation**.
If you would like to see what Drupal can do, check our [showcase](https://drupal-showcase.web.cern.ch/) for examples from across CERN!
Drupal itself is free and entirely open-source, developed with contributions from a sprawling ecosystem of thousands developers.
More information is available [here](https://www.drupal.org/).

![The home.cern website when logged in.](/assets/img/1-getting-started/getting_started_home_cern.png)

<p className="image-caption-shadow">The home.cern website when logged in.</p>

Almost every website at CERN is built using Drupal.

[https://home.cern/](https://home.cern/), as shown in the above screenshot, is no exception.
Accordingly, CERN maintains a suite of CERN-specific modules and themes which ensures a consistent look and feel across _.web.cern.ch_ and _.cern_ websites.
These modules and themes are included in the official **CERN Drupal Distribution**, which in turn is managed by the dedicated IT Drupal Infrastructure Team.
This gives every Drupal website created at CERN access to the same set of modules and themes out-of-the-box, vastly simplifying the process of developing a website.

### What is a module?

In the context of Drupal, a module is a software package which provides functionality not offered through Drupal itself.
At CERN, this could be integration with CDS ([https://cds.cern.ch/](https://cds.cern.ch/)) to host media or Indico ([https://indico.cern.ch/](https://indico.cern.ch/)) to import and display events.
By default, all CERN websites include a collection of CERN-specific modules through the centrally managed **CERN Drupal Distribution**.
In addition to official CERN modules developed and maintained by the Web Team, the distribution also includes a collection of carefully cuarated community contributed modules, ensuring that the most common use-cases are accommodated out-of-the-box.

While we always recommend websites to stick with what is included in the CERN Drupal Distribution, it is possible to install any number of modules made available to the wider Drupal community via [https://www.drupal.org/project/project_module](https://www.drupal.org/project/project_module) if such functionality is absolutely necessary.
Kindly note, however, that installing community contributed modules in this manner, or otherwise expanding the functionality offered in the CERN Drupal Distribution, requires website owners to assume responsibility of their website(s) moving forward.
This includes, but is not limited to, ensuring continued compatibility with Drupal and any underlying dependencies or frameworks that may change or be upgraded independent of the specific module(s) such as PHP, CKEditor, or Symfony.
We further strongly recommend creating internal documentation on any customisation to ensure future colleagues can maintain the website(s) in question.

All Drupal modules are written in PHP.

### What is a theme?

In the context of Drupal, a theme is styling that changes the look and feel of your website.

As with modules, a dedicated CERN Theme is included in the CERN Drupal Distribution.

All Drupal themes are written in CSS/SCSS and may utilise jQuery and JavaScript.

### What are the prerequisites to get started?

None!

While knowledge of PHP, HTML, CSS, jQuery, and JavaScript can be helpful, there is no need to know anything about programming to get started.
In fact, the Web Team (IR-ECO-PRD) has prepared a one-click Easy Start template containing everything you need.
Additionally, because Drupal is widely used across CERN, chances are someone in your team or group already has some experience!
If you have very specific requirements not immediately covered by any of the default modules, knowledge of PHP fast becomes necessary.

