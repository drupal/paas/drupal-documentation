---
slug: /creating-websites
---

# Creating Websites

All Drupal websites are created via the [Webservices Portal](https://webservices-portal.web.cern.ch/).

:::note
Webservices Portal has replaced [https://webservices.web.cern.ch/webservices/](https://webservices.web.cern.ch/webservices/).
The new portal is actively being developed and will be subject to a number of updates throughout 2022 and 2023.
If you created your website on the old portal, it will also be accessible via the new portal.
If you encounter any issues using the new portal, or would like to request a new feature, please [submit a ticket](https://cern.service-now.com/service-portal?id=service_element&name=web-portal).
:::

Anyone with a CERN account may create a website.

1. Access [the portal](https://webservices-portal.web.cern.ch/) and login.
   Click **Create** and choose **Drupal site**.

![The Webservices Portal frontpage.](/assets/img/1-getting-started/creating_websites_1.png)

<p className="image-caption-shadow">The Webservices Portal frontpage.</p>

![Creating a new website via the Webservices Portal.](/assets/img/1-getting-started/creating_websites_2-1.png)

<p className="image-caption-shadow">Creating a new website via the Webservices Portal.</p>

2. Add a name, e.g. `my-website`, and a description.
   You cannot change the name later.

Select the **Profile - CERN** option.
This includes everything you need to get started.

:::note
We recommend using a dedicated Drupal e-group for each website!
:::

Complete the process by specifying which e-group owns the website.

![Adding an administrator group to a new website.](/assets/img/1-getting-started/creating_websites_2.png)

<p className="image-caption-shadow">Adding an administrator group to a new website.</p>

3. With all fields completed, agree to the CERN Computing Rules and click **Create**.

![Creating a new website.](/assets/img/1-getting-started/creating_websites_3.png)

<p className="image-caption-shadow">Creating a new website.</p>

This will take you to the following confirmation page.

Please do not close the browser _before_ you have seen this screen.

![Confirmation screen that a new website has been created.](/assets/img/1-getting-started/creating_websites_4.png)

<p className="image-caption-shadow">Confirmation screen that a new website has been created.</p>

Congratulations! You have now created your very first Drupal website.

Your website is being allocated resources and **will be accessible within ~10 minutes**.

All future communication regarding your website will be sent to the specified e-group.
