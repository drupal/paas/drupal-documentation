---
slug: /backup-restore-websites
---

# Backup or Restore Websites

Backing up and restoring Drupal websites is done via the [Webservices Portal](https://webservices-portal.web.cern.ch/).

:::note
Webservices Portal has replaced [https://webservices.web.cern.ch/webservices/](https://webservices.web.cern.ch/webservices/).
The new portal is actively being developed and will be subject to a number of updates throughout 2022 and 2023.
If you created your website on the old portal, it will also be accessible via the new portal.
If you encounter any issues using the new portal, or would like to request a new feature, please [submit a ticket](https://cern.service-now.com/service-portal?id=service_element&name=web-portal).
:::

:::info
We strongly recommend taking backups before making changes to your production website.
This way, you can easily revert the changes if needed.
:::

Anyone with a CERN account and administrative access to a specific website may create a clone.

1. Open your browser and head to https://webservices-portal.web.cern.ch/. Sign in.

2. Click **My Sites**.

![The Webservices Portal frontpage.](/assets/img/1-getting-started/clone_website_1.png)

<p className="image-caption-shadow">The Webservices Portal frontpage.</p>

3. Search for the website you wish to backup or restore.

![Searching for a website on the Webservices Portal.](/assets/img/1-getting-started/clone_website_2.png)

<p className="image-caption-shadow">Searching for a website on the Webservices Portal.</p>

4. This page shows you ownership and other general information about your website.

![Adding a new environment.](/assets/img/1-getting-started/clone_website_3.png)

<p className="image-caption-shadow">Adding a new environment.</p>

5. Select the website you wish to backup or restore.

#### Backup 

If you wish to create a backup, enter a name for the backup and click `Create`. A backup will be scheduled.

![In order to backup, you must provide a name of the backup](/assets/img/1-getting-started/backup_website.png)

#### Restore

If you wish to restore from an existing backup, select the backup you wish to use from the drop-down menu and click `Restore`. A restore will be scheduled.

![To restore, click on the box next to 'Restore from backup' and chose the backup to restore from](/assets/img/1-getting-started/restore_website.png)

