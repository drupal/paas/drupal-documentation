---
slug: /cloning-websites
---

# Cloning Websites

Cloning Drupal websites is done via the [Webservices Portal](https://webservices-portal.web.cern.ch/).

:::note
Webservices Portal has replaced [https://webservices.web.cern.ch/webservices/](https://webservices.web.cern.ch/webservices/).
The new portal is actively being developed and will be subject to a number of updates throughout 2022 and 2023.
If you created your website on the old portal, it will also be accessible via the new portal.
If you encounter any issues using the new portal, or would like to request a new feature, please [submit a ticket](https://cern.service-now.com/service-portal?id=service_element&name=web-portal).
:::

:::info
We strongly recommend always creating and working on a clone before making changes to your production website.
This way, you can work without worrying about inadvertently breaking your production website.
Once satisifed with the results, you can apply the same changes on your production website with peace of mind.
:::

Anyone with a CERN account and administrative access to a specific website may create a clone.

1. Open your browser and head to https://webservices-portal.web.cern.ch/. Sign in.

2. Click **My Sites**.

![The Webservices Portal frontpage.](/assets/img/1-getting-started/clone_website_1.png)

<p className="image-caption-shadow">The Webservices Portal frontpage.</p>

3. Search for the website you wish to clone or create a test site for. Click the cog.

![Searching for a website on the Webservices Portal.](/assets/img/1-getting-started/clone_website_2.png)

<p className="image-caption-shadow">Searching for a website on the Webservices Portal.</p>

4. This page shows you ownership and other general information about your website.

In order to create a clone or a test site, click **Add Environment** under **Environments**.

![Adding a new environment.](/assets/img/1-getting-started/clone_website_3.png)

<p className="image-caption-shadow">Adding a new environment.</p>

5. Select the website you wish to clone from.

![Selecting which website to clone from.](/assets/img/1-getting-started/clone_website_4.png)

<p className="image-caption-shadow">Selecting which website to clone from.</p>

6. Give your new website a name, e.g. `<website-name>-test`.

Select the version of Drupal you need.

:::warning
Selecting a different Drupal version than the one you are cloning from may break your clone.
:::

Click **Create**.

![Creating a clone.](/assets/img/1-getting-started/clone_website_5.png)

<p className="image-caption-shadow">Creating a clone.</p>

It typically takes 10-15 minutes for a new website to be created.
