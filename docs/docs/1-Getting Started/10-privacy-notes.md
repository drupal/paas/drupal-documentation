---
slug: /cookie-policy
---

# Cookie Policy 

:::danger
The following information applies strictly to the CERN Drupal Distribution.
If you install and enable additional modules, you must ensure that your cookie policy is updated accordingly.
If, for instance, you install additional tracking modules, this **must** be reflected in your website's cookie policy.
:::

The following applies to all CERN websites:

> When you visit websites hosted at CERN, including reading or downloading material, CERN’s servers automatically record information (“log data”) which may include resource usage, IP addresses, information about what type of browser and operating system you are using and digital access information.
> On certain websites, including registration pages, recruitment pages and questionnaires, you may be asked to provide additional information, such as email addresses.
> Log data is collected through various technologies, including “cookies” and session data, whereby the following information is associated with the cookie: IP address, unique identifiers and status information. This data is also used to track your usage of CERN websites.

Source: https://home.cern/privacy

In the context of Drupal, the following cookies are enabled:

## Cookies Enabled by Default

In general, the content of a brand new Drupal website is determined by the CERN Drupal Distribution and which modules you then enable.
An overview of what is included in the composer by default can be found at https://gitlab.cern.ch/drupal/paas/cern-drupal-distribution/-/blob/master/composer.json.
In addition to this, the Drupal Core features a number of cookies (see, for example, https://api.drupal.org/api/drupal/9.3.x/search/cookie, sort by type class). 

The core functionality is unrelated to tracking and purely concerns functionality such as ensuring users stay logged in, remembering work when writing content and thus forth. 
The type of cookies included in the Drupal Core should not be thought of as cookies similar to those placed by Facebook or Google, but rather as an extension of the CMS itself to ensure the functionality you expect and require.

## Disable Cookies

You can disable everything if you want to, but core functionality (remembering you are logged in, for instance) may break.
If you start with a brand new Drupal website, there should be no need to disable any included functionality.
If, however, you enable modules such as Matomo (see https://matomo.org/), which is included in the CERN Drupal Distribution, you need to pay attention to what that means for privacy.
