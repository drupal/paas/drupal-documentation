---
slug: /web-governance
---

# Web Governance

A new federated structure is in place comprised of a Web governance board, a Web presence committee and Departmental Web Representatives​.

It ensures the alignment of CERN's web presence with the organisational strategy, makes recommendations, implements decisions and ensures a two way information flow between the central governance and the departments​.

## Web Representatives

The main responsibilities of the departmental web contact points is to ensure two-way information flow between the web presence committee and the departments, to have an overview of the departmental websites and to advise web owners within their department of best practices. 

**Activities**

- Liaising with the web presence committee and the web owners within their department to ensure a centralised two-way information flow.
- Being owners of mission-critical websites and therefore familiar with CERN’s web technologies and best practices.
- Acting as a web “superuser” so that the web presence committee can discuss changes and seek feedback ahead of web community meetings and documentation improvements.
- Maintaining an overview of the websites within their department.
- Advising members of their department prior to creating a website.
- Overseeing implementation of web presence recommendations.
- Assigning ownership of websites when web owners leave the organisation.

Web representatives are the first point of contact for website owners. They ensure CERN moves in a coherent fashion.

List of web representatives:

|        | **Department / Unit**                                     | **Representative**     | 
|--------|-----------------------------------------------------------|------------------------|
| DG     | Directorate                                               | Claudia Passeri        |
| ATS    | ATS                                                       | Eugenia Hatziangeli    |
| HSE    | Occupational Health & Safety and Environmental Protection | Ulla Tihinen           |
|   BE   |   Beams                                                   | Stephane Deghaye       |
|   EN   |   Engineering                                             | Catharina Hoch         |
|   SY   |   Accelerator Systems                                     | Katerina Leventaki   |
|   TE   |   Technology                                              | Germana Riddone        |
|   FAP  |   Finance and Administrative Processes                    | Caroline Laignel       |
|   HR   |   Human Resources                                         | Caroline Laignel       |
|   IPT  |   Industry, Procurement and Knowledge Transfer            | Caroline Laignel       |
|   SCE  |   Site and Civil Engineering                              | Caroline Laignel       |
|   IR   |   International Relations                                 | Almudena Solero        |
|   EP   |   Experimental Physics                                    | Joel Cloiser           |
|   IT   |   Information Technology                                  | Cath Noble             |
|   TH   |   Theoretical Physics                                     | Elena Gianolio         |

In addition, we have representatives for the following experiments.

|        | **Experiment**                                            | **Representative**     |
|--------|-----------------------------------------------------------|------------------------|
| ALICE     | ALICE                                                  | Ernesto Lopez Torres / Guillermo Mesa Perez   |
| ATLAS   | ATLAS | Steven Goldfarb / Karolos Potamianos             | Andreas Hoecker                 |
| CMS   |   CMS                                                      | Sofia Hurst / Dave Barney       |
| LHCb   |   LHCb                                                    | Joel Cloiser                    |
