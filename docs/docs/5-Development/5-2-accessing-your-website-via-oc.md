---
slug: /development/oc
---

# Access via `oc`

:::warning
This guide is for advanced users only.
If you are not comfortable working through the terminal, editing code, or overriding default options, please do not proceed.
If you are unsure whether you require the functionality outlined in this guide, please reach out via the community forums.
In order to get started with `oc`, elevated permissions must be requested by submitting a SNOW ticket to the IT Drupal Infrastructure Team here.
:::

## Authenticating

:::info
Please ensure that you have installed the OpenShift CLI on your computer before proceeding. The Red Hat OpenShift Getting Started Documentation features instructions for every major platform (Windows, MacOS X, and Linux): https://docs.openshift.com/container-platform/4.6/cli_reference/openshift_cli/getting-started-cli.html. The OpenShift CLI can be downloaded directly from: https://drupal.cern.ch/command-line-tools
:::

0. Open a terminal window.
   On MacOS, use Terminal.
   On Windows, use Powershell.
   On Linux, use any installed terminal.

If you have just installed the OpenShift CLI `oc`, simply proceed with the terminal window you have already opened.

1. Start by verifying that you have access to the oc command in your terminal.

If nothing shows when typing `oc` and hitting enter, please ensure you have installed the OpenShift CLI oc correctly and added `oc` to your PATH environment.

OR

If you do not have `oc` added to your PATH environment, or do not know what PATH is, navigate to the folder in which `oc` resides (e.g. type `cd ~/Downloads` and hit enter if `oc` is located in your `Downloads` folder) and type `./oc` and hit enter.
Kindly note that all subsequent calls containing `oc` must be prefixed with `./` and made in the folder `oc` is located.
If you continue to face issues, please reach out via the forums.

:::note
While it is easier to use `oc` when it is added to your PATH environment as this enables you to call oc from any folder, all subsequent actions necessary for this guide can be conducted using `./oc`.
If you would like to add `oc` to your PATH environment, please see [this guide](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/) for Windows, [this guide](https://code2care.org/howto/add-path-to-in-macos-big-sur) for MacOS, or [this guide](https://www.howtogeek.com/658904/how-to-add-a-directory-to-your-path-in-linux/) for Linux.
Adding `oc` to your PATH environment is thus not a requirement but a recommendation.
:::

![A MacOS terminal window running oc.](/assets/img/5-development/oc_1.png)

<p className="image-caption-shadow">A MacOS terminal window running oc.</p>

2. At this stage, we need to obtain a token in order to login.

:::note
Please ensure that you are connected through the CERN Network for this step.
**If you are teleworking, you thus need to either use a remote desktop, VPN, SSH tunnelling, or something similar.**
:::

Tokens are generated on-demand by accessing https://oauth-openshift.drupal.cern.ch/oauth/token/request.

![Generating a new token.](/assets/img/5-development/oc_2.png)

<p className="image-caption-shadow">Generating a new token.</p>

Click **Display Token**.

![Displaying the newly generated token.](/assets/img/5-development/oc_3.png)

<p className="image-caption-shadow">Displaying the newly generated token.</p>

Copy the content under **Log in with this token** and paste it in your terminal.
Hit enter.

![Pasting the oc login command.](/assets/img/5-development/oc_4.png)

<p className="image-caption-shadow">Pasting the oc login command.</p>

:::warning
Login via `oc login` is not possible due to 2FA. 
If you'd like to authenticate with your credentials please follow this [documentation](https://paas.docs.cern.ch/1._Getting_Started/5-installing-cli/#login-with-the-cli).
:::

## Accessing Projects

3. Once logged in, you receive a welcome message and may access your projects.

If you have previously accessed, you will also be reminded which project you are currently connected to.

Please note that in the context of OpenShift, a project is a [Kubernetes namespace](https://en.wikipedia.org/wiki/Kubernetes), which in turn merely means isolated resources (such as memory, processing power, and storage) allocated to a single cluster (i.e. an isolated environment).
In the vast majority of cases, the only thing running inside a project will be one or more Drupal websites.
If a Python application or similar has been deployed as part of a project, this, too, can be accessed by selecting the project in question.

![Authenticated and using a project.](/assets/img/5-development/oc_5.png)

<p className="image-caption-shadow">Authenticated and using a project.</p>

4. In order to see all available projects, type `oc projects` and hit enter.

5. In order to select a specific project, type `oc project <name_of_project>` and hit enter.

![Accessing a specific project.](/assets/img/5-development/oc_6.png)

<p className="image-caption-shadow">Accessing a specific project.</p>

In this case, we are connecting to the `th-dep` project.
Please ensure that you are connecting to the correct project.

## Accessing Pods

6. Inside an OpenShift project, one or more pods are running.
   In the vast majority of cases, a pod can be thought of as your Drupal website.

In reality, a pod can mean one or more containers deployed on our host.
A container in this context is a [Docker container](https://www.docker.com/resources/what-container), i.e. a standard unit of software which packages up code and all its dependencies, allowing an application to launch quickly and reliably across different computing environments.
In many ways, a container can be considered an application similar to other applications installed on your computer, except that everything needed to run the application is included directly.
In practice, this means that if you are running multiple applications as part of your specific project (e.g. a test website, a Python application, or something else entirely), these will show up as individual pods.

In order to see the current pods (i.e. the one or more containers currently deployed on our host), type `oc get pods`.
However, since we are only interested in seeing what is currently running, pipe the output into `grep` and filter for "Running".
The `grep` (global regular expression print) command searches through the output, looking for matches to the specified pattern.
In this case, which pods are currently running.

Type `oc get pods | grep Running` and hit enter.

![Grepping running pods on a specific project.](/assets/img/5-development/oc_7.png)

<p className="image-caption-shadow">Grepping running pods on a specific project.</p>

If you are seeing something unexpected in this output, please confirm that you are connected to the right project.
If you are on the right project, but still face issues, please verify that your site is not currently being built by typing `oc get pods` and hitting enter.
If you are on the right project, your site is not actively being built, but you still do not see what you expect, please [submit a ticket](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=Drupal-Service).

7. We are now ready to access the pod (i.e. website) of our interest, e.g. `th-dep-test-6bdcbff66d-nh4fs`.

The naming convention of pods may be confusing at first sight, but the random characters suffixed may be ignored entirely. Focus just on the name.

We access our chosen pod by typing `oc rsh -c php-fpm th-dep-test-6bdcbff66d-nh4fs` and hitting enter.

:::info
If you do not need to access a specific pod, but just want to access your website directly without any fuss, it is possible to write `oc rsh -c php-fpm deploy/website-name`.
In the case of `th-dep-test`, we would write `oc rsh -c php-fpm deploy/th-dep-test` and hit enter.
This connects you directly to your website.
Using this approach, there is no need to identify the specific pod using grep.
:::

Type `ls` and hit enter to see the content of `/app`.

![The content of /app.](/assets/img/5-development/oc_8.png)

<p className="image-caption-shadow">The content of /app.</p>

The `rsh` stands for remote shell session (see more information [here](https://www.mankier.com/1/oc-rsh)) and the `-c` argument specifies the container name
`php-fpm` (https://php-fpm.org/) is a PHP-specific implementation of [FastCGI](https://en.wikipedia.org/wiki/FastCGI) allowing us to interface with programs with a web server.
At this stage, you have access to the `composer` and `drush` for the accessed pod.

## Getting SQL Database credentials
To get SQL database credentials type:

`drush sql-connect`

You should receive this in return:

`mysql --user=<sql_username> --password=<sql_password> --database=<sql_database> --host=<cern_proxy> --port=<db_port> -A`