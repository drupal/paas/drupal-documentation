---
slug: /development/okd-ui
---

# Access via the `OKD` Web Interface

:::warning
This guide is for advanced users only.
If you are not comfortable working through the terminal, editing code, or overriding default options, please do not proceed.
If you are unsure whether you require the functionality outlined in this guide, please reach out via the community forums.
:::

## Accessing your project

1. Access the OpenShift portal and login with your CERN account: https://drupal.cern.ch/k8s/cluster/projects.

2. Select the project (i.e. website) whose WebDAV you wish to access.

![Selecting a project via the OpenShift interface.](/assets/img/5-development/okd_1.jpeg)

<p className="image-caption">Selecting a project via the OpenShift interface.</p>

3. If not already selected, change to **Administrator** by clicking the drop-down menu on the left-hand side.

![Selecting Administrator via the OpenShift interface.](/assets/img/5-development/okd_2.jpeg)

<p className="image-caption">Selecting Administrator via the OpenShift interface.</p>

4. Also in the menu on the left-hand side, unfold **Operators** and choose **Installed Operators**.

![Selecting operators via the OpenShift interface.](/assets/img/5-development/okd_3.jpeg)

<p className="image-caption">Selecting Operators via the OpenShift interface.</p>

5. Click on **Create Drupal Website** (and validate that you are indeed in the correct project).

![Selecting a project via the OpenShift interface.](/assets/img/5-development/okd_4.jpeg)

<p className="image-caption">Clicking 'Create Drupal Website' via the OpenShift interface.</p>

The button may have a confusing name, but clicking it takes you to the following page with a list of current sites.

6. Select "Current namespace only" and after select your website.

![Selecting the website via the OpenShift interface.](/assets/img/5-development/okd_5.png)

<p className="image-caption">Selecting the website via the OpenShift interface.</p>


7. Under the **YAML** tab at the top, you'll be able to edit specific configurations.

![Selecting YAML via the OpenShift interface.](/assets/img/5-development/okd_6.jpeg)

## Running commands inside a specific website

8. On the shown yaml, validate this is indeed the site you want to make your changes. This can be done by cross checking the "siteUrl" field ("spec" > "siteUrl"). If this includes the site you want to manage please memorize the field "metadata" > "name"

9. Go on the page Workloads > Pods and filter by "Running". You'll reach a page with a url such as https://drupal.cern.ch/k8s/ns/my-site/core~v1~Pod?rowFilter-pod-status=Running. Select the one that starts with the "metadata" > "name" field you memorized above.
Pods names follow the naming convention "drupalsite-name-hash-hash2".

For example, for a primary drupal site is named "my-site", there will be a matching pod named "my-site-7d596b9f58-mmctx". The hashes change frequently, please don't memorize them.

10. Click on the pod selected and then select the Terminal tab. For example, the above leads me to https://drupal.cern.ch/k8s/ns/my-site/pods/my-site-7d596b9f58-mmctx/terminal

11. On the dropdown "connecting to ... nginx" select php-fpm.

12. You can now run any necessary commands inside. Common commands are "drush updb" to run all required updates, followed by "drush cr" to refresh caches.
