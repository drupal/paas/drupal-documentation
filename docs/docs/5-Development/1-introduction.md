---
slug: /development
---

# Introduction

There are two ways in which you can access your website for development purposes.

1. WebDAV
2. The OpenShift CLI tool `oc`

There are two ways in which modules can be updated, via (1) WebDAV, or (2) via oc. 

In any case, the process of updating a module must be completed by the website owner(s) directly. 

**It is strongly recommended to always create a clone before attempting to update a module.**

If all goes well on the clone, steps can be repeated on your production website, or you can promote your clone to production by going to the Webservices Portal and promoting the clone to primary in the `Set as primary env` toggle.


