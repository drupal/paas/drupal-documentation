---
slug: /development/webdav-client
---

# WebDAV via a Client

:::info
File upload to WebDAV via Cyberduck is limited to 100MB. 
Above that limit, you should either compress them, or host them elsewhere such as [CDS](https://cds.cern.ch/) or [CERNBox](https://cernbox.cern.ch))
:::


Unlike Windows and Linux where the default file system supports WebDAV, MacOS' Finder is different on the new OpenShift cloud infrastructure.
In order to increase accessibility and general ease of use, we recommend downloading and installing the open-source client Cyberduck.
This can be done by accessing https://cyberduck.io/.
Kindly note that Cyberduck is available for both MacOS and Windows.
Users who wish to use a graphical user interface may thus prefer Cyberduck.
The necessary steps are identical across both platforms.

:::note
This guide assumes Cyberduck version 8.0.0 or later.
:::

![The Cyberduck.io frontpage.](/assets/img/5-development/cyberduck_1.png)

<p className="image-caption-shadow">The Cyberduck.io frontpage.</p>

1. With Cyberduck installed, proceed by opening it and creating a **New Bookmark**.

![Creating a new bookmark.](/assets/img/5-development/cyberduck_2.png)

<p className="image-caption-shadow">Creating a new bookmark.</p>

For the next step, please have your WebDAV password on hand.

2. Select **WebDAV (HTTPS)** in the top drop-down menu.
   If you have not unfolded **More Options**, do this too.

![Selecting WebDAV (HTTPS).](/assets/img/5-development/cyberduck_3.png)

<p className="image-caption-shadow">Selecting WebDAV (HTTPS).</p>

If, for instance, you wish to connect to `https://hr.cern.web.ch/`'s WebDAV, the following fields must be entered:

**Server**: `hr.web.cern.ch`
**Username**: `admin`
**Password**: `<randomly_generated_password>`
**Path**: `_webdav`

If you do not see the **Path** field, please unfold **More Options**.

![Configuring the WebDAV (HTTPS) connecting.](/assets/img/5-development/cyberduck_4.png)

<p className="image-caption-shadow">Configuring the WebDAV (HTTPS) connection.</p>

Close the settings window.

Double-click on the bookmark you just created to connect.

![Configuring the WebDAV (HTTPS) connecting.](/assets/img/5-development/cyberduck_5.png)

<p className="image-caption-shadow">Configuring the WebDAV (HTTPS) connection.</p>

You now have WebDAV access to your website.
