---
slug: /development/update-custom-modules
---

# [LEGACY] Update Custom Modules

:::danger Deprecated
As of 10th of April 2024, the Drupal service no longer supports the installation of community-contributed, or custom modules and themes for **new** websites. See [OTG0149420](https://cern.service-now.com/service-portal?id=outage&n=OTG0149420) for more details.
The existing documentation is kept solely to assist for websites existing before 10 April 2024.
:::

---

In the event that the modules provided via the [CERN Drupal Distribution](https://gitlab.cern.ch/drupal/paas/cern-drupal-distribution/-/blob/v9.4-1/composer.json#L103)) are not sufficient for your needs, you may:

- post on the [CERN Drupal Community Forums](https://drupal-community.web.cern.ch/) to see whether others have already achieved what you are looking for;
- submit a request to the Drupal Team to evaluate whether the feature(s) you require can be accommodated by adding a new module to the CERN Drupal Distribution, benefitting all CERN websites; or
- manually install a custom module directly on your website(s).

:::warning
Please note that installing and using custom modules requires both (a) continuous maintenance and (b) continuous attention to updates, security patches, and new feature releases.
Indeed, any website is a living and breathing piece of software.
This work will solely be the responsibility of the website owner(s) as any custom module not in the CERN Drupal Distribution will not benefit from the centrally managed updates.
If you require a set-and-forget solution, please do not exceed what is offered in the CERN Drupal Distribution.
:::

## Updating Modules

There are two ways in which modules can be updated, via (1) WebDAV, or (2) via `oc`.
In any case, the process of updating a module must be completed by the website owner(s) directly.
It is **strongly recommended** to always [create a clone](https://drupal.docs.cern.ch/docs/Getting%20Started/cloning-websites) before attempting to update a module.
If all goes well on the clone, steps should be repeated on your production website.

:::note
In both webdav and composer, users need to execute `drush updb` inside the running pod/container to ensure all configurations are updated.
Complete the process by typing `drush cr`.

See the following docs to learn [how to run commands inside a specific website](/development/okd-ui/#running-commands-inside-a-specific-website).
:::

### Updating Modules via WebDAV

:::note
We recommend experienced users to use `oc` and the `composer` directly.
:::

If you are unsure how to connect to your website's WebDAV, please refer to [this guide](https://drupal.docs.cern.ch/development/access-via-webdav).

The below screenshot shows an example of a mounted WebDAV folder.
All CERN websites share this structure.

![A mounted WebDAV folder of a CERN website.](/assets/img/5-development/update_modules_1.png)

<p class="image-caption-shadow">A mounted WebDAV folder of a CERN website.</p>

Navigate to the **modules** folder.

This folder contains all the modules currently installed on your website.

Kindly note that the names of the folders correspond to the machine names of the modules. Do not change these.

![The modules folder of a CERN website.](/assets/img/5-development/update_modules_2.png)

<p class="image-caption-shadow">The modules folder of a CERN website.</p>

At this stage, we need a local copy of the module we wish to update.
There are several ways in which this can be obtained; this guide will the two most common:
The **Extend** tab and the **Upgrade Status** report.
If you already happen to have a local copy of the module you wish to update, kindly skip the next two sections.

---

#### Using the Extend Tab

1. Go through the **Extend** tab and select **Update**.
   On this page, all modules with new(er) releases are listed.

![The Update tab under Extend.](/assets/img/5-development/update_modules_3.png)

<p class="image-caption-shadow">The Update tab under Extend.</p>

2. Click the **Release notes** link.
   In this case, we proceed with the `config_filter` module.

This opens a new tab or window in which the specific release can be downloaded directly. **Choose the .zip version.**

![Downloading a copy of a module.](/assets/img/5-development/update_modules_4.png)

<p class="image-caption-shadow">Downloading a copy of a module.</p>

#### Using the Upgrade Status Report

Another way of obtaining the most recent release of a specific module is to use the **Upgrade Status report**:

1. Access this by heading to **Report** and selecting the **Upgrade Status report** at the bottom.

![The Reports tab.](/assets/img/5-development/update_modules_5.png)

<p class="image-caption-shadow">The Reports tab.</p>

2. Scroll to the **Update** section to identify modules in need of update(s).

In this case, we only have the `quick_node_clone` module marked as in need of an update.

Click on the **Drupal.org version** number.

![Selecting a specific module.](/assets/img/5-development/update_modules_6.png)

<p class="image-caption-shadow">Selecting a specific module.</p>

This opens a new tab or window in which the specific release can be downloaded directly. **Choose the .zip version.**

![Downloading a copy of a module.](/assets/img/5-development/update_modules_7.png)

<p class="image-caption-shadow">Downloading a copy of a module.</p>

---

At this stage, we have a copy of the module saved directly on our machine as a .zip file. **Unzip the module**.

Open the WebDAV window once again and drag-n-drop the folder into the **modules** folder in your WebDAV window.

Note how in the screenshot below, we are dragging the _unzipped_ folder.

![Dragging the unzipped module onto your server.](/assets/img/5-development/update_modules_8.png)

<p class="image-caption">Dragging the unzipped module onto your server.</p>

This will prompt a window asking you to confirm the overwrite. Select **Continue**.

![Overwriting the old copy of the module.](/assets/img/5-development/update_modules_9.png)

<p class="image-caption-shadow">Overwriting the old copy of the module.</p>

Once the transfer has finished, the module in question has been updated accordingly.

If you are updating modules through the Upgrade Status report, re-scan the module(s) you have updated to verify the update.

Repeat this process for each module in need of an update.

### Updating Modules via Composer

Modules can be updated in the same way as they are installed. Check the docs on [installing a custom module via composer](/development/custom-modules#installing-modules-via-composer).

If you wish to upgrade the modules on just a clone and not all the environments, you may create a new branch and point a clone to this version. Specifically explained [here](/development/custom-modules?_highlight=branch&_highlight=repositoryurl#fill-the-extra-configs-field) where to configure this new branch.

