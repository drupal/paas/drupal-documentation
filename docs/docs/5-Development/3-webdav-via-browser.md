---
slug: /development/webdav-browser
---

# WebDAV via a Browser

:::note
Accessing WebDAV via the browser is only recommended to make changes to a single file at a time.
:::

:::info
File upload to WebDAV via Browser is limited to 8MB. Too upload larger files, you should use [Cyberduck](https://drupal.docs.cern.ch/development/webdav-client) which has a limit of 100MB. 
Above that limit, you should either compress them, or host them elsewhere such as [CDS](https://cds.cern.ch/) or [CERNBox](https://cernbox.cern.ch))
:::

In order to access your website's WebDAV through a browser, go to `https://<your_website>/_webdav`.

A pop-up prompting you for your username and password will show.

1. Enter username `admin`.
2. Enter as password the `randomly_generated_password` you copied from [step eight](https://drupal.docs.cern.ch/development/access-via-webdav).
