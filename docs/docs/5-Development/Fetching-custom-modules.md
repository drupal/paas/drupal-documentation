1. Access the Website via Cyberduck (WebDAV)
- Use Cyberduck to connect to WebDAV.
- Guide on how to connect using Cyberduck: [Cyberduck - WebDAV](https://drupal.docs.cern.ch/development/webdav-client)
- Guide for retrieving the WebDAV password: [WebDAV Password Retrieval](https://drupal.docs.cern.ch/development/access-via-webdav)

2. Navigating to Custom Modules
- Once connected via WebDAV, navigate to the modules folder.
- In the modules folder, look for the custom modules sub-folder. 
- If no sub-folder exists, custom modules are within the main modules folder.

![Modules folder](/assets/img/5-development/Modules_folder-cyberduck.png)

![Custom module](/assets/img/5-development/Custom-module.png)

3. Removing Custom Modules
- You can remove the custom directly from Cyberduck by deleting the desired module.

Summary
To fetch custom modules, connect to the website via Cyberduck using WebDAV. Navigate to the modules folder to locate custom modules, then remove them directly through Cyberduck.

