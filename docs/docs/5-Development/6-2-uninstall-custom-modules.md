---
slug: /development/uninstall-custom-modules
---

# [LEGACY] Uninstall Custom Modules

:::danger Deprecated
As of 10th of April 2024, the Drupal service no longer supports the installation of community-contributed, or custom modules and themes for **new** websites. See [OTG0149420](https://cern.service-now.com/service-portal?id=outage&n=OTG0149420) for more details.
The existing documentation is kept solely to assist for websites existing before 10 April 2024.
:::

---
In the event that the modules provided via the [CERN Drupal Distribution](https://gitlab.cern.ch/drupal/paas/cern-drupal-distribution/-/blob/v9.4-1/composer.json#L103)) are not sufficient for your needs, you may:

- post on the [CERN Drupal Community Forums](https://drupal-community.web.cern.ch/) to see whether others have already achieved what you are looking for;
- submit a request to the Drupal Team to evaluate whether the feature(s) you require can be accommodated by adding a new module to the CERN Drupal Distribution, benefitting all CERN websites; or
- manually install a custom module directly on your website(s).

:::warning
Please note that installing and using custom modules requires both (a) continuous maintenance and (b) continuous attention to updates, security patches, and new feature releases.
Indeed, any website is a living and breathing piece of software.
This work will solely be the responsibility of the website owner(s) as any custom module not in the CERN Drupal Distribution will not benefit from the centrally managed updates.
If you require a set-and-forget solution, please do not exceed what is offered in the CERN Drupal Distribution.
:::

## Uninstalling Modules

While it is possible to also just remove the modules from webdav or composer directly, it is recommended to go through the Drupal interface directly.
This approach has the added benefit of displaying warnings in the event that published content will be impacted by the removal of the module(s) in question, 
and removing associated configurations from the database.

:::note
We recommend also executing `drush updb` inside the running pod/container to ensure the database does not contain any module configurations that are no longer necessary.
Complete the process by typing `drush cr`.
:::

1. Go to the **Extend** tab and click **Uninstall**.

![The Extend tab.](/assets/img/5-development/uninstall_1.png)

<p class="image-caption-shadow">The Extend tab.</p>

![The Extend tab.](/assets/img/5-development/uninstall_2.png)

<p class="image-caption-shadow">The Uninstall section under the Extend tab.</p>

2. Search or scroll through the list for the module(s) you wish to uninstall and remove.

Tick the module(s) and scroll to the bottom, click **Uninstall**.

3. Confirm that the chosen module(s) will be uninstalled.

![Confirm the removal of the chosen module(s).](/assets/img/5-development/uninstall_3.png)

<p class="image-caption-shadow">Confirm the removal of the chosen module(s).</p>

Click **Uninstall**.

4. Confirm that the module(s) have been removed.

![Confirming the uninstallation and removal of the chosen module(s).](/assets/img/5-development/uninstall_4.png)

<p class="image-caption-shadow">Confirming the uninstallation and removal of the chosen module(s).</p>
