---
slug: /development/access-database
---

# Access Database

:::warning
This guide is for advanced users only.
Working directly with your database is a dangerous and unsupported workflow.
If you are not comfortable working through the terminal, editing code, or overriding default options, please do not proceed.
If you are unsure whether you require the functionality outlined in this guide, please reach out via the community forums.
In order to get started with `oc`, elevated permissions must be requested by submitting a SNOW ticket to the IT Drupal Infrastructure Team [here](https://cern.service-now.com/service-portal?id=sc_cat_item&name=request&se=Drupal-Service).
:::

This guide assumes that you have a MySQL client (either CLI-based or graphical) installed on your machine.
This guide further assumes that you are on the CERN network as outside connections are blocked by the CERN firewall.
If you do not have `oc` available on your machine, please see [this guide](https://drupal.docs.cern.ch/development/oc) for more information.

1. Connect to the Drupal cluster by executing the following command:

`oc login --server=https://api.drupal.okd.cern.ch -u <username>`

where `username` is your CERN usename.

2. Extract the database connection information for your website by executing the following command:

`oc -n PROJECT exec -c php-fpm deploy/MYSITE -- drush sql-connect`

where `PROJECT` is your project and `MYSITE` is the specific website.

This gives an example output as follows:

```
mysql --user=0d4f580 --password=hSnc52hSN7C3 --database=0d4f580 --host=dbod-ha-proxy.cern.ch --port=7857 -A
```

3. Use this information to connect to your database using your preferred database tool.

If, for instance, you are using **DBeaver**, you would

(a) Create a new connection

![Creating a new connection.](/assets/img/5-development/access_database_1.png)
<p className="image-caption">Creating a new connection.</p>

(b) Select MySQL

![Selecting MySQL.](/assets/img/5-development/access_database_2.png)
<p className="image-caption">Selecting MySQL.</p>

(c) Enter the values from step 2.

![Entering connection settings.](/assets/img/5-development/access_database_3.png)
<p className="image-caption">Entering connection settings.</p>

(d) Add SSH details to access when not on the CERN network.

![Adding SSH information to access outside of CERN.](/assets/img/5-development/access_database_4.png)
<p className="image-caption">Adding SSH information to access outside of CERN.</p>

(e) Select the database(s) of interest.

![Selecting the database(s) of interest.](/assets/img/5-development/access_database_5.png)
<p className="image-caption">Selecting the database(s) of interest.</p>
