---
slug: /development/best-practices-long-term
---

# Best Practices

In this article we are going to speak about **universal pain-points** and drupal-specific practices only, since long-term best practices are:

- Hard to specify
- Flexibility to respond to whatever comes next.
- Consistent setup that is easy to follow.
- Solid foundation that minimizes future limitations.
- Low friction for existing and new team members.
    - Shorten the onboarding process.
    - Reduce the necessary background knowledge.

:::note
The main aim of this section is to provide a way of looking at things, **guidelines rather than instructions**.
:::

## Field Strategy

Fields are attached to entities, and they store data. They are important due to the fact that:

- Every project will use them.
- Fields define the structure.
- Belong to the business logic.
- Affect many areas of the site.
- Hard to structurally change later.

For that reasons you should consider the following key-points when you are going to plan your *field strategy*:

- Machine name
    - Cannot be changed, only migrated to a new field.
    - Will be used everywhere.

- Shared fields vs dedicated fields
    - What can and what cannot be shared.
    - Shared fields indirectly can share logic and styles.

:::success
- Plan ahead.

- Understand the pros and cons of shared fields *vs* dedicated fields.

- Choose the naming carefully
    - Verbose is easier to understand than abbreviated, keep always in mind long-term & new people.
    - Consistent pattern, *For example:* if you use plural in fields naming for indicate fields that accept more than one item, use always plural in all these situations.

- Document everything!
:::


## Entity Strategy

Entities are “containers” of your structure such as Nodes, Blocks or any custom data you want to deal with. When yoy are planning your strategy keep in mind the following considerations:

- Cern Core / drupal core vs custom entities:
    - **Creating own comes with maintenance**, you will be able to assume the workload involved on it?
    - Using existing modules is easier but requires adjustments to your use-case.

- Logical grouping of data:
    - Entity types vs bundles.
    - Shared or singular purpose of each entity; e.g. “Article”.

- Custom content vs configuration entities:
    - Where should the data live – DB vs configuration.
    - Managing fields through the Field UI.

:::success
- Avoid sharing entities for different purposes:
    - Hard to maintain.
    - Will result in overrides to cover all use-cases.

- Choose the naming carefully:
    - Verbose is easier to understand than abbreviated.
    - Consistent pattern.

- Understand the different options you have:
    - Core/contrib vs custom.
    - Custom content vs configuration.
    - Separate entity types vs bundles.

- Document everything!
:::


## Roles and Permissions Strategy

**Roles are "Job titles"** in the website and usually they are "set and forget" and tend to end up in a mess or overshare. To avoid that you can follow **two different approaches:**

- Role-centric
    - Revolves around the role of the user.
    - Used by default.
    - Administrator, editor, manager, subscriber, etc.

- Permission-centric
    - Revolves around capabilities, rather than the role.
    - Each role is a single (or group of tightly related) capabilities on the site like Manage configuration, Manage content, Manage users.

:::success
- Role-centric:
    - Traditional approach. Majority of the sites will benefit from this.
    - Automatically appropriate when having different types of front-end users.

- Permission-centric:
    - Many different levels of users with backend access.
    - When combining permissions per user account is more desired.
    - Hard to manage for too many areas, better then to group into roles.
:::


## Quick Mention - Rely on core

Chosing Cern Core and Drupal Core over contributed modules or customizations, when it is possible, will make your life easier since

- Core will always be more future-proof.
- Lower number of contrib modules is easier to maintain.


And for that, be sure to understand what is given with core:
- You can achieve a lot with what Drupal gives out of the box.
- Useful for creating own features.
- Useful as a blueprint.














