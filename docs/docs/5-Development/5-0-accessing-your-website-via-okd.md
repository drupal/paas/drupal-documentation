---
slug: /development/okd
---

# OpenShift

:::warning
This guide is for advanced users only.
If you are not comfortable working through the terminal, editing code, or overriding default options, please do not proceed.
If you are unsure whether you require the functionality outlined in this guide, please reach out via the community forums.
:::

## Introduction

As of November 2021, all CERN websites are served by the OpenShift cloud infrastructure.

While this has little-to-no impact on day-to-day content creation through Drupal compared to the old infrastructure, the migration to OpenShift allows access to a number of powerful tools that can ease both website management and development such as the Drupal Shell Drush (https://www.drush.org/latest/) and the PHP dependency manager Composer (https://getcomposer.org/).
These, and potentially other tools, are accessed either by the Openshift Interface ([https://drupal.cern.ch](https://drupal.cern.ch)) or by using the OpenShift Command Line Interface (CLI), `oc`.
If you are unfamiliar with these tools, please refer to their respective websites to attain a quick overview.

Working via the terminal using `oc`, `composer`, and `drush` is not required.
Indeed, users who prefer not to may proceed with general Drupal development using the familiar WebDAV as on the old infrastructure: However, it is nonetheless strongly encouraged that users familiarise themselves with the available tools.
The main motivation for this includes automated dependency management, reduced manual work, correct versioning, and easier access to modules and themes.

This guide serves strictly as an introduction, outlining how to access website(s) through the web interface.
As such, and particularly considering the range of features available through both composer and drush, users with more advanced requirements are kindly referred to the respective documentations directly for more information.

