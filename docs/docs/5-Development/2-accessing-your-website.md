---
slug: /development/access-via-webdav
---

# Access FileSystem (WebDAV)

:::note
This method is recommended for _beginner_ and _intermediate_ users.
:::

## Retrieve your WebDAV Password

As part of the migration to OpenShift, additional security measures have been employed.
In practice, this means that the way in which you access your website's WebDAV has changed slightly and now requires new credentials.
If you have configured WebDAV prior to the OpenShift migration, please remove any existing configurations before continuing.
Once you have retrieved your new credentials, mounting or otherwise accessing WebDAV remains unchanged.


1. Follow the steps indicated under [https://drupal.docs.cern.ch/development/okd-ui](https://drupal.docs.cern.ch/development/okd-ui) to access your project. 

2. Click the **YAML** tab at the top.

![Selecting YAML via the OpenShift interface.](/assets/img/5-development/okd_6.jpeg)

<p className="image-caption">Selecting YAML via the OpenShift interface.</p>

3. You will now be able to find your WebDAV password.

`CTRL + F` (or `CMD + F` on MacOS) for `webDAVPassword`, or merely scroll a bit down in the file.
Copy the password.

![Copying the WebDAV password via the OpenShift interface.](/assets/img/5-development/webdav_pass.jpeg)

<p className="image-caption">Copying the WebDAV password via the OpenShift interface.</p>

**Please do not share this password!**
