---
slug: /themes/cern-theme
---

# CERN Theme

A theme is what defines the look and feel of your website.
It specifies regions in which you assign content, placement of menus and key elements of the website.
A Drupal theme contains templates (which can be also specialised for specific content types), stylesheets, and may contain various libraries and JavaScript for advanced animations.

At CERN, the **CERN Theme** is provided in order to set a similar look and feel to the websites of the organisation.
The CERN Theme is the recommended Drupal theme because it defines a common look and feel across all CERN websites.
Indeed, it is the default theme included in the CERN Drupal Distribution.

![A website using the CERN Theme as seen via the Apperance tab.](/assets/img/3-cern-themes/cern_theme_enabled.png)

<p className="image-caption-shadow">A website using the CERN Theme as seen via the Apperance tab.</p>

## Theme Settings

In order to access the settings of a theme, click the **Apperance** menu.

![The CERN Theme settings page.](/assets/img/3-cern-themes/cern_theme_settings.png)

<p className="image-caption-shadow">The CERN Theme settings page.</p>

### Modifying Basic Settings

In order to modify basic settings of the CERN Theme (i.e. _Header_ and _Footer_ content), the checkboxes in the first section of the Settings page is used.

Specifically, under _Header_:

- **Show Site name** determines whether or not the site name is rendered.
- **Show Site slogan** determines whether or not the site slogan is rendered.
- **Show Search link on Main menu** determines whether a search link is included in the main menu.
- **Site Logo** allows the upload of a custom logo.

And, under _Footer_:

- **Show Dark LOGO on footer** determines whether or not a dark CERN logo is used in footer (default is light).

:::note
If you opt for a light-colored footer background on your footer, consider using a dark CERN logo to ensure the logo remains visible.
In a similar vein, please ensure that the colour of your text remains readable regardless of background colour.
:::

### Colours

The CERN Theme allows you to modify the colour palette of your website.

![The CERN Theme settings page.](/assets/img/3-cern-themes/cern_theme_settings_2.png)

<p className="image-caption-shadow">The CERN Theme settings page.</p>

## Theme Regions

The CERN Theme has a total of nine regions.

![An overview of the CERN Theme's regions.](/assets/img/3-cern-themes/theme_regions.png)

<p className="image-caption">An overview of the CERN Theme's regions.</p>

### <span style={{color: `red`}}>Header</span>

The header region is placed on the top of the website and includes elements like the logo, the site name, and the main menu that you can set from the theme settings.

### <span style={{color: `yellow`}}>Left Sidebar</span>

The left sidebar region holds 3 of the 12 underlying Bootstrap columns and can be used to place menus and views.
On mobile devices, the sidebar is placed above the Main Content region.

### <span style={{color: `green`}}>Right Sidebar</span>

The right sidebar region holds 3 of the 12 underlying Bootstrap columns and can be used to mainly place menus and views.
On mobile devices, the sidebar is placed after the Main Content region.

### <span style={{color: `blue`}}>Main Content</span>

The main content region constitutes the region that holds the main information of a page.
Moreover, one can place extra views and blocks within the region.
It holds 6 of the 12 underlying Bootstrap columns and can be used to place the main content of the page.
In the event where the left sidebar or right sidebar is absent (e.g. if no blocks are placed in those regions), the main content holds 9 of the 12 underlying Bootstrap columns.

### <span style={{color: `orange`}}>Content Footer</span>

The Content Footer region holds 12 of the 12 underlying Bootstrap columns and is placed under the Left Sidebar, Main Content, and Right Sidebar regions.
It can be used to mainly place views like the Horizontal Boxes carousel.

### <span style={{color: `purple`}}>Footer</span>

The footer constitutes a branding element of the websites that use the CERN Theme.
The common elements of the Footer are the CERN logo, the copyright text, and the four regions of the footer.
The footer itself can be used to place quick links in order to help the users navigate to external websites (e.g. social media).
The footer consists of four regions, each holding 3 of the 12 underlying Bootstrap columns.

## Blocks and Menus

Menus in the CERN theme are rendered in a different way, based on where they are placed.

### Header Block

In order to add menu items on the header, refer to the Main Navigation menu (Structure > Menu > Main Navigation).
The header items are rendered using a specific font, size and animation.
More specifically, whenever the user hovers over a menu item, an animation is triggered in order to show the current pointer position.
In a similar way, when a user is located in a page, the underlined item indicates the current page of the user.

The GIF below shows how the menu behaves while hovering above items and expanding multiple levels.

![The behaviour of the menu block.](/assets/img/3-cern-themes/block_header_gif.gif)

<p className="image-caption">The behaviour of the menu block.</p>

:::note
A common issue is that the second level of a menu item is not rendered even though it is added in the menu structure.
This is solved by enabling the "Show as expanded" option of the parent menu item.
:::

### Sidebar Block

Two types of sidebars exist in the CERN Theme.

#### Menus

Menus placed on sidebar feature specific characteristics:

- First level menu items are discretely bigger than the rest
- The current page is always underlined
- When the user hovers over a menu item, the item is underlined
- The menu inherits the background colour of the header to make it more visible.

The GIF below presents the aforementioned elements.

![The behaviour of the menu sidebar block.](/assets/img/3-cern-themes/block_sidebar_gif.gif)

<p className="image-caption">The behaviour of the menu sidebar block.</p>

#### Custom Blocks and Views

When a custom block or a view is placed on a sidebar, it has a shadow border around it so that the borders between the blocks will be discrete.

**Blocks without shadows**

The shadow around a block can be avoided by adding the class `.without-shadow` in the block.
In order to add a class in a block using the drupal admin interface:

1. Install and enable the [Block class module](https://www.drupal.org/project/block_class)
2. Visit the block in the block layout settings
3. At the bottom of the page, add `without-shadow` in _CSS class(es)_ setting

![Example of blocks placed in a sidebar with and without borders.](/assets/img/3-cern-themes/block_sidebar_border.png)

<p className="image-caption">Example of blocks placed in a sidebar with and without borders.</p>

### Footer

The footer constitutes a branding element of the websites that use the CERN Theme.
The elements of the footer region are the CERN Logo, the Copyright text and the four regions of the footer.
The footer can be used to place quick links in order to help the users navigate to external websites.
It can also be used to place links of Social Media.
The latter is easily achievable by using the CERN Full HTML text format along with the CERN font.

To create footer menus, you need to follow the steps below:

1. Create a Menu in `/admin/structure/menu`
2. Add the menu items in the menu
3. Place the menu in one of the four footer regions/columns. This can be achieved by visiting `/admin/structure/block` and clicking **Place Block** in the region that you want to place it.

The GIF below shows how menus behave when placed on the footer of a website.

![The behaviour of the footer menu.](/assets/img/3-cern-themes/block_footer_menu.gif)

<p className="image-caption">The behaviour of the footer menu.</p>

## Tables

In order to make a table responsive when using the CERN Theme, the table needs to be wrapped around a `div` that includes the class `.table-responsive`.

```html
<div class="table-responsive">
  <table>
    <!-- table content here -->
  </table>
</div>
```
