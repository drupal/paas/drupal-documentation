---
slug: /themes/cern-colours
---

# CERN Colours

Another important feature of the CERN Theme are the options provided in the appearance page. 
If you browse the settings of CERN Theme in `/admin/appearance/settings/cernclean`, you will find a set of options which define how all the already mentioned elements will look like. 
You have the option to set up the color palette of your website for a pre-defined set of elements like headers, footers, tables, tabs etc.

| **Element**                              | **Description**                                                       |
|------------------------------------------|-----------------------------------------------------------------------|
| Header Background                        | Background color of the header                                        |
| Header: Site name text                   | Site name color                                                       |
| Menu: Link                               | Color of menu links                                                   |
| Body: text                               | Color of the text within the body                                     |
| Body: link                               | Color of the links within the body                                    |
| Body: link on hover                      | Color of the link within the body when the user hovers over the link. |
| Body: Background                         | Background color of body                                              |
| Body: Line in u tag &amp; list bullets   | Underline color and list bullets color                                |
| Body: Caption text                       | Caption text color within body                                        |
| Tabs: Active tab background              | Background color of active tab (currently selected tab)               |
| Tabs: Active tab text                    | Text color of active tab                                              |
| Tabs: Inactive tab background            | Background color of inactive tab                                      |
| Tabs: Inactive tab text                  | Text color of inactive tab                                            |
| Table: Background                        | Background color of tables                                            |
| Table: Header background                 | Header background of tables                                           |
| Table: Header text                       | Header text of tables                                                 |
| Table: Rows even background              | Background color of even rows                                         |
| Table: Rows odd background               | Background color of odd rows                                          |
| Table: Rows text                         | Text color of tables' rows                                            |
| Table: Footer background                 | Background color of tables' footer                                    |
| Table: Footer text                       | Text color of tables' footer                                          |
| Icons: Slider navigation active          | Color of active navigation bulltes                                    |
| Icons: Slider navigation inactive        | Color of inactive navigation bullets                                  |
| Icons: Chevrons &amp; animated underline | Color of chevrons and animated line under links                       |
| Icons: Blockquotes                       | Color of blockquotes icon                                             |
| Icons: Slider navigation arrows active   | Active navigation arrows                                              |
| Icons: Slider navigation arrows inactive | Inactive navigation arrows                                            |
| Views: Card background                   | Background color of cards.                                            |
| Views: Card title                        | Card title color of views                                             |
| Views: Card text                         | Card text color of views                                              |
| Taxonomy terms: Tag Background           | Background color of taxonomy terms                                    |
| Taxonomy terms: Tag text                 | Text color of taxonomy terms                                          |
| Buttons: border                          | Border color of buttons                                               |
| Buttons / Primary: background            | Background color of primary buttons                                   |
| Buttons / Primary: text                  | Text color of primary buttons                                         |
| Buttons / Primary: background hover      | Background color on hover of primary buttons                          |
| Buttons / Primary: text hover            | Text color on hover of primary buttons                                |
| Buttons / Secondary: background          | Background color of secondary buttons                                 |
| Buttons / Secondary: text                | Text color of secondary                                               |
| Buttons / Secondary: background hover    | Background color on hover of secondary buttons                        |
| Buttons / Secondary: text hover          | Text color on hover of secondary buttons                              |
| Footer: Background                       | Background color of footer                                            |
| Footer: Text                             | Text color of footer                                                  |
| Footer: Link                             | Link color of footer                                                  |
| Footer: Link on hover                    | Link color on hover                                                   |
