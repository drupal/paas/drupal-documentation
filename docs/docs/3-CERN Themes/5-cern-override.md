---
slug: /themes/cern-override-theme
---

# [LEGACY] CERN Override Theme

:::warning Deprecated
As of 10th of April 2024, the Drupal service no longer supports the installation of community-contributed, or custom modules and themes for **new** websites. See [OTG0149420](https://cern.service-now.com/service-portal?id=outage&n=OTG0149420) for more details.
The existing documentation is kept solely to assist for websites existing before 10 April 2024.
:::

---

We strongly recommend only using the default `CERN Theme`. 
Indeed, this theme includes functionality designed specifically to fit the needs of most CERN websites. 
However, if you, for one reason or another, do not find the functionality that you require, it _is_ possible to override the default `CERN Theme` locally on your website. 
This is done by installing a special version of the theme called the `CERN Override Theme`.

:::warning
Please note that installing and using custom themes requires both (a) continuous maintenance and (b) continuous attention to updates, security patches, and new feature releases.
Indeed, any website is a living and breathing piece of software.
This work will solely be the responsibility of the website owner(s) as any custom module not in the CERN Drupal Distribution will not benefit from the centrally managed updates.
If you require a set-and-forget solution, please do not exceed what is offered in the CERN Drupal Distribution.
:::

## Installation

The `CERN Override Theme` is managed by the Web Team. 

In order to access and subsequently install the `CERN Override Theme`:

1. Self-subscribe to the `drupal-override-theme-users` e-group (see https://e-groups.cern.ch/e-groups/EgroupsSearchForm.do). You can subscribe either yourself directly or add the dedicated `drupal-admins-<WEBSITE_NAME>` e-group if you happen to be an administrator.
2. Grab the theme file using one of the following ways: If you are familiar with terminal, execute `git clone https://gitlab.cern.ch/web-team/drupal/public/d10/themes/cern-override` and proceed accordingly. If you are not familiar with terminal, download the `.zip` file with the theme files via https://gitlab.cern.ch/web-team/drupal/public/d10/themes/cern-override/-/archive/master/cern-override-master.zip.
3. Install the theme as you would a [custom module](https://drupal.docs.cern.ch/development/custom-modules), only this time the `cernoverride` folder should be placed under the dedicated `/themes` folder such that the final structure matches `/themes/cernoverride`. If your structure does not match, you will be unable to enable the theme. If your website does not have an existing `/themes` folder, create one.
4. Visit `https://your-website.web.cern.ch/admin/appearance` and set the `CERN Override Theme` as default.
5. Clear the caches. Done!

## Using CERN Override

The `CERN Override Theme` allows you to add additional functionality on top of the `CERN Theme`.

You may make changes to the `CERN Theme` via customised stylesheets or scripts (e.g. `.css` or `.js` files), or by adding snippets of CSS code to the pre-existing `theme.css` file. 
Indeed, the file structure of the `CERN Override Theme` is similar to that of any other Drupal theme. 
You will thus find several predefined files and folders such as the `/css/theme.css` file and the dedicated `/css/color` folder. 
Anything added to the `theme.css` file, for instance, will be applied at the very end to override any settings set by the default theme. 

### Theme Inheritance

Any theme in Drupal can inherit settings, characteristics, styles, templates from another theme. 
The theme that is inherited is called parent theme or base theme and the theme that is inherits is called child theme or sub-theme. 
At CERN, the `CERN Base` inherits from `Bootstrap` while the `CERN Theme` inherits from `CERN Base`. 
On a day-to-day basis, you will only interact with the `CERN Theme`. 
Finally, and entirely optionally, the `CERN Override Theme` can be installed and will inherit from the `CERN Theme`. 
This is the reason why applying changes in the `CERN Override Theme` allows you to override settings.

### Example

There are several ways in which the `CERN Override Theme` may be used. 

Imagine that you wish to have headers on your website appear with a box-shadow.
As this is not something which is normally supported by the `CERN Theme` directly, you have to override it.
As box-shadow constitutes a styling configuration, this is applied by making changes to the `theme.css` file in the `CERN Override Theme`.
One way of doing this would be to add the following snippet to the `theme.css` file:

```css
.header{
     -webkit-box-shadow: 0px 0px 0px 5px rgba(0,0,0,0.75);
     -moz-box-shadow: 0px 0px 0px 5px rgba(0,0,0,0.75);
     box-shadow: 0px 0px 0px 5px rgba(0,0,0,0.75);
}
```

Saving the file and updating the copy on your website will now produce a box-shadow on headers.
