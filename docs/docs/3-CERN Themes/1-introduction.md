---
slug: /themes
---

# Introduction to Themes

Themes defines the look and feel of a website.

In Drupal, the look of a specific website is determined by which theme is enabled.
At CERN, all websites feature the same core elements (a fixed header and footer) via a dedicated **CERN Theme** to ensure a common look and feel across websites.
This is the default theme included in the CERN Drupal Distribution.
The theme, and Drupal itself, is responsive and thus accommodates both computers, tablets, and phones out-of-the-box.

By default, new websites automatically include this theme alongside a number of official CERN-modules.

![Showcase of the CERN Theme on home.cern.](/assets/img/3-cern-themes/cern_theme.png)

<p className="image-caption-shadow">Showcase of the CERN Theme on home.cern.</p>

Included in the CERN Drupal Distribution are the following CERN-official themes:

- [drupal/cern-theme](https://gitlab.cern.ch/web-team/drupal/public/d10/modules) 
- [drupal/cern-adminimal-subtheme](https://gitlab.cern.ch/web-team/drupal/public/d10/themes)
- [drupal/cern-base-theme](https://gitlab.cern.ch/web-team/drupal/public/d10/themes)
