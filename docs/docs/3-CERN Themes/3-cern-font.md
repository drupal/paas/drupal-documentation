---
slug: /themes/cern-font
---

# CERN Font

The CERN Font provides a set of icons which can be added through typing.
In order to use the CERN Font, you need to just expand the font selection and choose the icons font.
After selecting the font, you can use your keyboard to type icons instead of letters.
In this way, you are able to add icons in your text.
You can find a table of the available icons below.

![The CERN Font icons.](/assets/img/3-cern-themes/cern_font_icon_mapping.png)

<p className="image-caption">The CERN Font icons.</p>
