/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: "Drupal Documentation",
  tagline: "Documentation for Drupal @ CERN",
  url: "https://drupal.docs.cern.ch",
  baseUrl: "/",
  onBrokenLinks: "warn",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/drupal.ico",
  organizationName: "Drupal",
  projectName: "https://gitlab.cern.ch/drupal/paas/drupal-documentation",
  clientModules: [
    require.resolve('./static/assets/js/gantt_chart.js'),
  ],
  themeConfig: {
    announcementBar: {
        id: 'customisation_block',
        content:
          'As of April 10th 2024, installing custom modules in **new** websites is not supported. All documentation in reference to this is marked as legacy.',
        backgroundColor: '#f3a07a',
        textColor: '#fff',
        isCloseable: false,
      },
    navbar: {
      title: "Drupal Documentation",
      logo: {
        alt: "Drupal Logo",
        src: "img/drupal-logo.svg",
      },
      items: [
        {
          to: "/",
          activeBasePath: "docs",
          label: "Getting Started",
          position: "left",
        },
        {
          to: "/web-governance/",
          activeBasePath: "docs",
          label: "Governance",
          position: "right",
        },
        {
          href: "https://gitlab.cern.ch/drupal/paas/drupal-documentation",
          label: "Git",
          position: "right",
        },
      ],
    },
    footer: {
      style: "dark",
      logo: {
        alt: "CERN Logo",
        src: "img/cern_logo_footer.png",
        height: "40px",
      },
      links: [
        {
          title: "Docs",
          items: [
            {
              label: "Getting Started",
              to: "/",
            },
          ],
        },
        {
          title: "Outreach",
          items: [
            {
              label: "Talks & Presentations",
              to: "/publications",
            },
          ],
        },
        {
          title: "Community",
          items: [
            {
              label: "Drupal Forum",
              href: "https://drupal-community.web.cern.ch/",
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} CERN.`,
    },
  },
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          editUrl: ({ docPath }) =>
            `https://gitlab.cern.ch/drupal/paas/drupal-documentation/edit/master/docs/docs/${docPath}`,
          //path: 'docs',
          routeBasePath: "/", // Serve the docs at the site's root
          breadcrumbs: true,
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
  // from https://github.com/easyops-cn/docusaurus-search-local
  themes: [
    // ... Your other themes.
    [
      require.resolve("@easyops-cn/docusaurus-search-local"),
      {
        // ... Your options.
        // `hashed` is recommended as long-term-cache of index file is possible.
        hashed: true,
        highlightSearchTermsOnTargetPage: true,
	indexDocs: true, // Default is true, but let's force it
	indexPages: true,

        // For Docs using Chinese, The `language` is recommended to set to:
        // ```
        // language: ["en", "zh"],
        // ```
      },
    ],
  ],
};
